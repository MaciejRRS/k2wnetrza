<?php 
/* 
Template Name: Strona Główna
*/ 
?>

<?php get_header() ;?>

<main id="homepage">
    <section class="hero">
        <div class="container">
            <div class="hero-wrapper">
                <div class="column-left">
                    <h1><?php the_field('tytul_sekcja_1_home') ?></h1>
                    <?php the_field('tekst_sekcja_1_home') ?>
                    <div class="buttons-wrapper">

                        <?php if( get_field('przycisk_nazwa_sekcja_1_home') ): ?>
                        <a href="<?php the_field('przycisk_link_sekcja_1_home') ?>"
                            class="btn btn-grad"><?php the_field('przycisk_nazwa_sekcja_1_home') ?></a>
                        <?php endif; ?>

                        <?php if( get_field('przycisk2_nazwa_sekcja_1_home') ): ?>
                        <a href="<?php the_field('przycisk_link2_sekcja_1_home') ?>"
                            class="btn btn-grad bgWhite"><?php the_field('przycisk2_nazwa_sekcja_1_home') ?></a>
                        <?php endif; ?>

                    </div>
                </div>
                <div class="column-right">
                    <?php $imageHeroHomepage = get_field('zdjecie_sekcja_1_home'); ?>
                    <img class="img-hero-homepage" src="<?php echo $imageHeroHomepage['sizes']['large']; ?>"
                        width="<?php echo $imageHeroHomepage['sizes']['large-width']; ?>"
                        height="<?php echo $imageHeroHomepage['sizes']['large-height']; ?>"
                        alt="<?php echo esc_attr($imageHeroHomepage['alt']); ?>" />

                    <div class="animate-images">
                        <?php $imageHeroHomepageDesctop = get_field('imageHeroHomepageDesctop_animate'); ?>
                        <img class="home_desktop_img slide-in-fwd-center-desktop"
                            src="<?php echo $imageHeroHomepageDesctop['sizes']['large']; ?>"
                            width="<?php echo $imageHeroHomepageDesctop['sizes']['large-width']; ?>"
                            height="<?php echo $imageHeroHomepageDesctop['sizes']['large-height']; ?>"
                            alt="<?php echo esc_attr($imageHeroHomepageDesctop['alt']); ?>" />



                        <?php $imageHeroHomepageMobile = get_field('imageHeroHomepageMobile_animate'); ?>
                        <img class="home_desktop_mobile slide-in-elliptic-right-fwd-mobile"
                            src="<?php echo $imageHeroHomepageMobile['sizes']['large']; ?>"
                            width="<?php echo $imageHeroHomepageMobile['sizes']['large-width']; ?>"
                            height="<?php echo $imageHeroHomepageMobile['sizes']['large-height']; ?>"
                            alt="<?php echo esc_attr($imageHeroHomepageMobile['alt']); ?>" />



                        <?php $imageHeroHomepageTablet = get_field('imageHeroHomepageTablet_animate'); ?>
                        <img class="home_desktop_tablet slide-in-elliptic-right-fwd"
                            src="<?php echo $imageHeroHomepageTablet['sizes']['large']; ?>"
                            width="<?php echo $imageHeroHomepageTablet['sizes']['large-width']; ?>"
                            height="<?php echo $imageHeroHomepageTablet['sizes']['large-height']; ?>"
                            alt="<?php echo esc_attr($imageHeroHomepageTablet['alt']); ?>" />

                        <img class="img-animate-hp-circle infinite_opacity"
                            src="/app/themes/k2wnetrza/assets/src/img/circle_animate.svg">
                        <img class="img-animate-hp-polygon infinite_opacity_two"
                            src="/app/themes/k2wnetrza/assets/src/img/polygon_animate.svg">
                        <img class="img-animate-hp-line infinite_opacity_third"
                            src="/app/themes/k2wnetrza/assets/src/img/line_animate.svg">




                    </div>

                </div>
                <!-- <a href="#zachwyc-swoich-klientow"
                    class="arrow-icon-scroll"><span><?php the_field('scroll_napis','options') ?></span></a> -->
            </div>

        </div>


        <div class="box-partners">
            <div class="container">
                <h3><?php the_field('tytul_sekcji_z_partnerami_sekcja_1_home') ?></h3>

                <div class="partners-list-static">
                    <!-- partners > 991px -->
                    <?php
                if( have_rows('lista_partnerow_sekcja_1_home') ):
                    while( have_rows('lista_partnerow_sekcja_1_home') ) : the_row(); ?>
                    <div class="img-item-wrapper">
                        <?php $heroHomePartnerlogo = get_sub_field('logo_partnera_sekcja_1_home'); ?>
                        <img class="img" src="<?php echo $heroHomePartnerlogo['sizes']['thumbnail']; ?>"
                            width="<?php echo $heroHomePartnerlogo['sizes']['thumbnail-width']; ?>"
                            height="<?php echo $heroHomePartnerlogo['sizes']['thumbnail-height']; ?>"
                            alt="<?php echo esc_attr($heroHomePartnerlogo['alt']); ?>" />
                    </div>
                    <?php
                endwhile;
                else :
                endif;
                ?>

                    <!-- end partners > 991px -->
                </div>

                <!-- Swiper -->
                <div class="partners-slider">
                    <div class="swiper mySwiper-partners">
                        <div class="swiper-wrapper">
                            <?php
                if( have_rows('lista_partnerow_sekcja_1_home') ):
                    while( have_rows('lista_partnerow_sekcja_1_home') ) : the_row(); ?>
                            <div class="swiper-slide">
                                <div class="img-item-wrapper">
                                    <?php if( $heroHomePartnerlogo ): ?>
                                    <?php $heroHomePartnerlogo = get_sub_field('logo_partnera_sekcja_1_home'); ?>
                                    <img class="img" src="<?php echo $heroHomePartnerlogo['sizes']['thumbnail']; ?>"
                                        width="<?php echo $heroHomePartnerlogo['sizes']['thumbnail-width']; ?>"
                                        height="<?php echo $heroHomePartnerlogo['sizes']['thumbnail-height']; ?>"
                                        alt="<?php echo esc_attr($heroHomePartnerlogo['alt']); ?>" />
                                    <?php endif; ?>
                                </div>
                            </div>
                            <?php
                endwhile;
                else :
                endif;
                ?>

                        </div>

                    </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>
            </div>

        </div>

    </section>








    <?php
                    if( have_rows('lista_blokow_repeater_homepage') ): $i = 1;
                        while( have_rows('lista_blokow_repeater_homepage') ) : the_row(); ?>

    <section id="zachwyc-swoich-klientow" class="section-repeater section-repeater-<?php echo $i++; ?>">

        <div class="container">

            <div class="wrapper-repeater" data-aos="fade-up" data-aos-duration="1000">
                <div class="column-1">
                    <?php
                $spacer_zdjecie_home = get_sub_field('zdjecie_blok_repeater_homepage'); 
                $spacer_film_home = get_sub_field('film_blok_repeater_homepage'); 
                        
                $get_selection_home = get_sub_field('wybor_zdjecie_film_blok_repeater_homepage'); 

                 if ($get_selection_home == 'zdjecie'){ ?>
                    <a href="<?php the_sub_field('link_przycisku_blokow_repeater_homepage') ?>">
                        <?php $HomeRepeaterImg = get_sub_field('zdjecie_blok_repeater_homepage'); ?>


                        <?php if( $HomeRepeaterImg ): ?>
                        <img class="HomeRepeaterImg" src="<?php echo $HomeRepeaterImg['sizes']['large']; ?>"
                            width="<?php echo $HomeRepeaterImg['sizes']['large-width']; ?>"
                            height="<?php echo $HomeRepeaterImg['sizes']['large-height']; ?>"
                            alt="<?php echo $HomeRepeaterImg['alt']; ?>" />
                        <?php endif; ?>



                    </a>
                    <?php  } else if ($get_selection_home == 'film') { ?>
                    <div class="movie-area popup-area">
                        <?php the_sub_field('film_blok_repeater_homepage'); ?>
                    </div>
                    <img class="img-apla-serv"
                        src="<?php the_field('tlo_pod_filmem_blok_services_single','options') ?>">

                    <!-- start popup -->
                    <?php  } else if ($get_selection_home == 'popup') { ?>
                    <!-- <div class="movie-area popup-area btn-noAnimate">
                        <?php $HomeRepeaterImgPopup = get_sub_field('popup_blok_repeater_homepage'); ?>
                        <div id="myBtn" class="popup-clicable-area" data-toggle="modal"
                            data-target="#modal-box-<?php echo $i++; ?>">
                            <div class="icon-play">
                                <img src="<?php the_field('wybierz_ikonę_linku_popup','options') ?>">
                            </div>
                            <img class="HomeRepeaterImg" src="<?php echo $HomeRepeaterImgPopup['sizes']['large']; ?>"
                                width="<?php echo $HomeRepeaterImgPopup['sizes']['large']; ?>"
                                height="<?php echo $HomeRepeaterImgPopup['sizes']['large']; ?>"
                                alt="<?php echo esc_attr($HomeRepeaterImgPopup['alt']); ?>" />
                        </div>
                        <div id="modal-box-<?php echo --$i; ?>" class="modal">

                            <div id="mymodal-close" class="modal-content">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <iframe src="<?php the_sub_field('popup_repeater_homepage_content'); ?>"
                                    frameborder="0"></iframe>
                            </div>

                        </div>

                    </div> -->

                    <a href="<?php the_sub_field('popup_repeater_homepage_content'); ?>"
                        class="movie-area popup-area btn-noAnimate">
                        <?php $HomeRepeaterImgPopup = get_sub_field('popup_blok_repeater_homepage'); ?>
                        <div id="myBtn" class="popup-clicable-area" data-toggle="modal"
                            data-target="#modal-box-<?php echo $i++; ?>">
                            <div class="icon-play">
                                <img src="<?php the_field('wybierz_ikonę_linku_popup','options') ?>">
                            </div>
                            <img class="HomeRepeaterImg" src="<?php echo $HomeRepeaterImgPopup['sizes']['large']; ?>"
                                width="<?php echo $HomeRepeaterImgPopup['sizes']['large']; ?>"
                                height="<?php echo $HomeRepeaterImgPopup['sizes']['large']; ?>"
                                alt="<?php echo esc_attr($HomeRepeaterImgPopup['alt']); ?>" />
                        </div>
                    </a>
                    <img class="img-apla-serv"
                        src="<?php the_field('tlo_pod_filmem_blok_services_single','options') ?>">
                    <?php  } ?>
                    <!-- end popup -->

                </div>
                <div class="column-2">
                    <a href=" <?php the_sub_field('link_przycisku_blokow_repeater_homepage') ?>">
                        <div class="title-area">
                            <h2><?php the_sub_field('tytul_blok_repeater_homepage') ?></h2>
                        </div>
                        <div class="text-area">
                            <?php the_sub_field('tekst_blok_repeater_homepage_kopia') ?>
                        </div>
                    </a>
                    <?php if( get_sub_field('tekst_przycisku_blokow_repeater_homepage') ): ?>
                    <div class="btn-area">
                        <a href="<?php the_sub_field('link_przycisku_blokow_repeater_homepage') ?>"
                            class="btn-grad btn-repeater"><?php the_sub_field('tekst_przycisku_blokow_repeater_homepage') ?></a>
                    </div>
                    <?php endif; ?>
                </div>
            </div>

        </div>
    </section>
    <?php
                    endwhile;
                    else :
                    endif;
                    ?>





    <section class="cta-home" data-aos="fade-up" data-aos-duration="1000">
        <div class="cta-wrapper">
            <div class="container">
                <div class="title-area">
                    <h2><?php the_field('tytul_sekcja_z_kontaktem_Allepage','options') ?></h2>
                </div>
                <div class="text-area">
                    <?php the_field('tekst_sekcja_z_kontaktem_Allpage','options') ?>
                </div>
                <div class="btn-area">
                    <a href="<?php the_field('link_przycisku_sekcja_z_kontaktem_Allpage','options') ?>"
                        class="btn btn-grad"><?php the_field('tekst_przycisku_sekcja_z_kontaktem_Allpage','options') ?></a>
                </div>
            </div>
        </div>
    </section>


</main>


<?php get_footer();?>