<?php 
/* 
Template Name: Kontakt  
*/ 
?>

<?php get_header() ?>
<main id="contact">
    <section class="hero subpage">
        <div class="container">
            <div class="hero-wrapper">
                <div class="column-left" data-aos="fade-right" data-aos-duration="1000">
                    <h1><?php the_title(); ?></h1>
                    <div class="wrapper-for-columns">
                        <div class="column">
                            <h2><?php the_field('tytul_kolumna_1_contact_k2') ?></h2>
                            <?php the_field('tekst_kolumna_1_contact_k2') ?>
                        </div>
                        <div class="column">
                            <h2><?php the_field('tytul_kolumna_2_contact_k2') ?></h2>
                            <a class="link-w" href="<?php the_field('adres_linku_contact_k2') ?>"><img
                                    src="<?php the_field('ikona_wybor_contact_k2') ?>"><?php the_field('nazwa_linku_contact_k2') ?></a>
                            <a class="link-w" href="<?php the_field('adres2_linku_contact_k2') ?>"><img
                                    src="<?php the_field('ikona2_wybor_contact_k2') ?>"><?php the_field('nazwa_linku2_contact_k2') ?></a>
                        </div>
                        <div class="column">
                            <h2><?php the_field('tytul_kolumna_3_contact_k2') ?></h2>
                            <div class="wrapper-social-media-icon">
                                <a href="<?php the_field('ikona_facebook_contact_k2_link') ?>"><img class="img-sm"
                                        src="<?php the_field('ikona_facebook_contact_k2') ?>"></a>
                                <a href="<?php the_field('ikona_indeed_contact_k2_link') ?>"><img class="img-sm"
                                        src="<?php the_field('ikona_indeed_contact_k2') ?>"></a>
                                <a href="<?php the_field('ikona_instagram_contact_k2_link') ?>"><img class="img-sm"
                                        src="<?php the_field('ikona_instagram_contact_k2') ?>"></a>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="column-right mobile" data-aos="fade-left" data-aos-duration="1000">
                    <div class="form-area">
                        <?php echo do_shortcode(get_field('formularz_kontaktowy_contact')); ?>
                    </div>
                    <div class="wrap-img-contact mobile" data-aos="fade-right" data-aos-duration="1000">
                        <?php $imageHeroContact = get_field('zdjecie_contact_glowne'); ?>
                        <img class="img-hero-contact" src="<?php echo $imageHeroContact['sizes']['large']; ?>"
                            width="<?php echo $imageHeroContact['sizes']['large-width']; ?>"
                            height="<?php echo $imageHeroContact['sizes']['large-height']; ?>"
                            alt="<?php echo esc_attr($imageHeroContact['alt']); ?>" />
                    </div>
                </div>

            </div>
            <div class="breadcrumps">
                <?php if( function_exists( 'bcn_display' ) ) bcn_display(); ?>
            </div>
            <div class="wrap-img-contact" data-aos="fade-right" data-aos-duration="1000">
                <?php $imageHeroContact = get_field('zdjecie_contact_glowne'); ?>
                <img class="img-hero-contact" src="<?php echo $imageHeroContact['sizes']['large']; ?>"
                    width="<?php echo $imageHeroContact['sizes']['large-width']; ?>"
                    height="<?php echo $imageHeroContact['sizes']['large-height']; ?>"
                    alt="<?php echo esc_attr($imageHeroContact['alt']); ?>" />
            </div>
        </div>
    </section>

</main>

</main>
















<?php get_footer() ?>