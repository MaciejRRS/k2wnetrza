<?php 
/* 
Template Name: Realizacje z filtrowaniem     
*/ 
?>

<?php get_header() ?>
<main id="realizations" class="realization">
    <section class="hero hero-subpage">
        <div class="container">
            <div class="hero-wrapper">
                <div class="column-left" data-aos="fade-right" data-aos-duration="1000">
                    <h1><?php the_field('tytul_hero_description_realizacje');?></h1>
                    <?php the_field('tekst_hero_description_realizacje') ?>
                    <div class="buttons-wrapper">
                        <?php if( get_field('przycisk_nazwa_btn1_realizacje') ): ?>
                        <a href="<?php the_field('przycisk_link_btn1_realizacje') ?>"
                            class="btn btn-grad"><?php the_field('przycisk_nazwa_btn1_realizacje') ?></a>
                        <?php endif; ?>
                        <?php if( get_field('przycisk_tekst_btn2_realizacja') ): ?>
                        <a href="<?php the_field('przycisk_link_btn2_lista_realizacja');?>"
                            class="btn-grad bgWhite"><?php the_field('przycisk_tekst_btn2_realizacja') ?></a>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="column-right" data-aos="fade-left" data-aos-duration="1000">
                    <?php $imageHeroRealizations = get_field('zdjecie_sekcja_1_realizacje_img'); ?>
                    <img class="img-hero-homepage" src="<?php echo $imageHeroRealizations['sizes']['large']; ?>"
                        width="<?php echo $imageHeroRealizations['sizes']['large-width']; ?>"
                        height="<?php echo $imageHeroRealizations['sizes']['large-height']; ?>"
                        alt="<?php echo esc_attr($imageHeroRealizations['alt']); ?>" />
                </div>
            </div>
            <div class="breadcrumps">
                <?php if( function_exists( 'bcn_display' ) ) bcn_display(); ?>
            </div>
        </div>
        <a href="#wirtualny-spacer-nasze-realizacje" class="arrow-icon-scroll">
            <div class="arrow-area">
                <?php the_field('scroll_napis','options') ?> <i class="arrow down"></i>
            </div>
        </a>

    </section>


    <section id="wirtualny-spacer-nasze-realizacje" class="realizations" data-aos="fade-up" data-aos-duration="1000">

        <div class="container">

            <div class="title-area">
                <h2><?php the_field('naglowek_filter_realizacje_lista') ?></h2>
            </div>




            <div class="filter-area">
                <?php echo do_shortcode('[searchandfilter id="223"]') ?>
            </div>
        </div>
        <?php echo do_shortcode('[searchandfilter id="223" show="results"]') ?>
    </section>

    <?php if( get_field('tekst_co_mozesz_zyskac_realizacje_lista') ): ?>
    <section class="section_no_image" data-aos="fade-up" data-aos-duration="1000">
        <div class="section_no_image_wrapper">
            <div class="container">
                <div class="title-area">
                    <h2><?php the_field('tytul_co_mozesz_zyskac_realizacje_lista') ?></h2>
                </div>
                <div class="text-area">
                    <?php the_field('tekst_co_mozesz_zyskac_realizacje_lista') ?>
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>


    <section class="cta-home" data-aos="fade-up" data-aos-duration="1400">
        <div class="cta-wrapper">
            <div class="container">
                <div class="title-area">
                    <h2><?php the_field('tytul_sekcja_z_kontaktem_Allepage','options') ?></h2>
                </div>
                <div class="text-area">
                    <?php the_field('tekst_sekcja_z_kontaktem_Allpage','options') ?>
                </div>
                <div class="btn-area">
                    <a href="<?php the_field('link_przycisku_sekcja_z_kontaktem_Allpage','options') ?>"
                        class="btn btn-grad"><?php the_field('tekst_przycisku_sekcja_z_kontaktem_Allpage','options') ?></a>
                </div>
            </div>
        </div>
    </section>

</main>
















<?php get_footer() ?>