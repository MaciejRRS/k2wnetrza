<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>
        <?php wp_title(); ?>
    </title>
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>

    <?php wp_head(); ?>
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-WYG2065YB9"></script>
    <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-WYG2065YB9');
    </script>

</head>

<body>
    <div class="wrapper-primary">
        <header>

            <?php  if ( is_page_template( 'umow-spacer.php' ) || is_page_template('default-page.php')  || is_singular('post')) {
   
?>
            <nav class="navbar navbar-expand-lg navbar-top nav-subpage">

                <?php } else { ?>
                <nav class="navbar navbar-expand-lg navbar-top">
                    <?php } ?>



                    <div class="container">
                        <!-- custom logo start -->
                        <?php 
                    $custom_logo_id = get_theme_mod( 'custom_logo' );
                     $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                                    ?>
                        <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>"
                            title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"> <img
                                src="<?php echo $image[0]; ?>" alt=""></a>
                        <!--  custom logo stop -->

                        <!-- Collapse button burger menu Start-->
                        <button class="navbar-toggler first-button" type="button" data-toggle="collapse"
                            data-target="#navbarNavDropdown" aria-controls="navbarSupportedContent20"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <div class="animated-icon1"><span></span><span></span><span></span></div>
                        </button>
                        <!-- Collapse button burger menu End-->
                        <!-- start menu primary  -->
                        <?php
                    wp_nav_menu( array(
                        'theme_location'    => 'primary',
                        'depth'             => 2,
                        'link_before'    => '<span class="">',
                        'link_after'     => '</span>',
                        'container'         => 'div',
                        'container_class'   => 'collapse navbar-collapse',
                        'container_id'      => 'navbarNavDropdown',
                        'menu_class'        => 'nav navbar-nav',
                        'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                        'walker'            =>  new WP_Bootstrap_Navwalker(),
                    ) );
                ?>
                        <!-- end menu primary  -->

                        <?php if( get_field('wlacz_przelacznik_jezyka_na_stronach_k2wnetrza','options') ) { ?>
                        <div class="fixed-lang">
                            <?php do_action('wpml_add_language_selector'); ?>
                        </div>
                        <?php } ?>
                    </div>
                </nav>


                <?php if( get_field("popup_true", 'option') ): ?>
                <div class="header-fixed-button">
                    <button id="header-fixed-btn"
                        style="background-image: url(<?php the_field('fixed_btn_img', 'option')?>">
                        <?php if( get_field("popup_tel_true", 'option') ): ?>
                        <div class="fixed-tel-icon">
                            <img src="<?php the_field("popup_tel", "option");?>" />
                        </div>
                        <?php endif; ?>
                    </button>
                </div>

                <div class="fixed-popup" id="fixed-popup">
                    <button id="fixed-close"> X </button>
                    <img src="<?php the_field("popup_img", 'option');?>" />
                    <div class="popup-text">
                        <?php the_field("popup_text", 'option');?>
                    </div>
                    <div class="popup-btn">
                        <a href="<?php the_field("popup_url", 'option');?>">
                            <button class="btn-grad"><?php the_field("popup_btn", 'option');?></button>
                        </a>
                    </div>
                </div>
                <?php endif; ?>


        </header>