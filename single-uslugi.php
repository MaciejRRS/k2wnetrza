<?php get_header() ;?>


<?php $realization_excerpt = get_the_excerpt($post->ID);?>

<main id="services">
    <section class="hero hero-subpage">
        <div class="container">
            <div class="hero-wrapper">
                <div class="column-left" data-aos="fade-right" data-aos-duration="1000">
                    <h1><?php the_title(); ?></h1>
                    <?php echo '<p>'.$realization_excerpt.'</p>' ?>
                    <div class="buttons-wrapper">
                        <?php if( get_field('przycisk_nazwa_sekcja_1_single_services') ): ?>
                        <a href="<?php the_field('przycisk_link_sekcja_1_single_services') ?>"
                            class="btn btn-grad"><?php the_field('przycisk_nazwa_sekcja_1_single_services') ?></a>
                        <?php endif; ?>

                        <?php if( get_field('przycisk_tekst_uslugi_lista_realizacja') ): ?>
                        <a href="<?php the_field('przycisk_link_uslugi_lista_realizacja');?>"
                            class="btn-grad bgWhite"><?php the_field('przycisk_tekst_uslugi_lista_realizacja') ?></a>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="column-right" data-aos="fade-left" data-aos-duration="1000">
                    <?php $imageHeroSingleServices = get_field('zdjecie_sekcja_1_single_services'); ?>
                    <img class="img-hero-homepage" src="<?php echo $imageHeroSingleServices['sizes']['large']; ?>"
                        width="<?php echo $imageHeroSingleServices['sizes']['large-width']; ?>"
                        height="<?php echo $imageHeroSingleServices['sizes']['large-height']; ?>"
                        alt="<?php echo esc_attr($imageHeroSingleServices['alt']); ?>" />
                </div>
            </div>
            <div class="breadcrumps">
                <?php if( function_exists( 'bcn_display' ) ) bcn_display(); ?>
            </div>
        </div>

        <a href="#wirtualny-spacer-zobacz-nasze-uslugi" class="arrow-icon-scroll">
            <div class="arrow-area">
                <?php the_field('scroll_napis','options') ?> <i class="arrow down"></i>
            </div>
        </a>

    </section>



    <section id="wirtualny-spacer-zobacz-nasze-uslugi" class="section_no_image" data-aos="fade-up"
        data-aos-duration="1000" data-aos-duration="1000">
        <div class="section_no_image_wrapper">
            <div class="container">
                <div class="title-area">
                    <h2><?php the_field('tytul_sekcja_bez_zdjecia_service_single') ?></h2>
                </div>
                <div class="text-area">
                    <?php the_field('tekst_sekcja_bez_zdjecia_service_single') ?>
                </div>
            </div>
        </div>
    </section>


    <?php
                    if( have_rows('lista_blokow_repeater_services_single') ): $i = 2;
                        while( have_rows('lista_blokow_repeater_services_single') ) : the_row(); ?>


    <?php if( get_sub_field('wlaxzwylacz_blok_rep_service_single_block') ) { ?>
    <section class="section-repeater section-repeater-<?php echo $i++; ?> first-left-img">
        <div class="container">
            <div class="wrapper-repeater" data-aos="fade-up" data-aos-duration="1000">

                <div class="column-2">
                    <div class="title-area">
                        <h2><?php the_sub_field('tytul_blok_repeater_services_single') ?></h2>
                    </div>
                    <div class="text-area">
                        <?php the_sub_field('tekst_blok_repeater_services_single') ?>
                    </div>
                    <div class="btn-area">
                        <?php if( get_sub_field('przycisk_sprawdz_oferte_tekst_k2_services_single') ): ?>
                        <a href="<?php the_sub_field('przycisk_sprawdz_oferte_link_k2_services_single');?>"
                            class="btn-grad btn-repeater"><?php the_sub_field('przycisk_sprawdz_oferte_tekst_k2_services_single') ?></a>
                        <?php endif; ?>

                        <?php if( get_sub_field('przycisk_sprawdz_realizacje_tekst_k2_services_single') ): ?>
                        <a href="<?php the_sub_field('przycisk_sprawdz_realizacje_link_k2_services_single');?>"
                            class="btn-grad btn-repeater bgWhite"><?php the_sub_field('przycisk_sprawdz_realizacje_tekst_k2_services_single') ?></a>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="column-1">

                    <?php
$spacer_zdjecie = get_sub_field('zdjecie_blok_services_single'); 
$spacer_film = get_sub_field('film_blok_services_single'); 
$get_selection = get_sub_field('wybor_zdjecie_film_blok_services_single');

                if ($get_selection == 'zdjecie'){
                    $ServiceSingleRepeaterImg = get_sub_field('zdjecie_blok_services_single'); ?>
                    <img class="HomeRepeaterImg" src="<?php echo $ServiceSingleRepeaterImg['sizes']['large']; ?>"
                        width="<?php echo $ServiceSingleRepeaterImg['sizes']['large']; ?>"
                        height="<?php echo $ServiceSingleRepeaterImg['sizes']['large']; ?>"
                        alt="<?php echo esc_attr($ServiceSingleRepeaterImg['alt']); ?>" />
                    <?php  } else if ($get_selection == 'film') { ?>
                    <div class="movie-area">
                        <?php the_sub_field('film_blok_services_single'); ?>
                    </div>
                    <img class="img-apla-serv"
                        src="<?php the_field('tlo_pod_filmem_blok_services_single','options') ?>">


                    <!-- strt popup -->
                    <?php  } else if ($get_selection == 'popup') { ?>
                    <!-- <div class="movie-area popup-area btn-noAnimate">
                        <?php $SingleServiceRepeaterImgPopup = get_sub_field('SingleServiceRepeaterImgPopup'); ?>
                        <div id="myBtn" class="popup-clicable-area" data-toggle="modal"
                            data-target="#modal-box-<?php echo $i++; ?>">
                            <div class="icon-play">
                                <img src="<?php the_field('wybierz_ikonę_linku_popup','options') ?>">
                            </div>
                            <img class="HomeRepeaterImg"
                                src="<?php echo $SingleServiceRepeaterImgPopup['sizes']['large']; ?>"
                                width="<?php echo $SingleServiceRepeaterImgPopup['sizes']['large']; ?>"
                                height="<?php echo $SingleServiceRepeaterImgPopup['sizes']['large']; ?>"
                                alt="<?php echo esc_attr($SingleServiceRepeaterImgPopup['alt']); ?>" />
                        </div>
                        <div id="modal-box-<?php echo --$i; ?>" class="modal">

                            <div id="mymodal-close" class="modal-content">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <iframe src="<?php the_sub_field('popup_repeater_single_services_content'); ?>"
                                    frameborder="0"></iframe>
                            </div>

                        </div>

                    </div> -->

                    <a href="<?php the_sub_field('popup_repeater_single_services_content'); ?>" target="_blank"
                        class="movie-area popup-area btn-noAnimate">
                        <?php $SingleServiceRepeaterImgPopup = get_sub_field('SingleServiceRepeaterImgPopup'); ?>
                        <div id="myBtn" class="popup-clicable-area" data-toggle="modal"
                            data-target="#modal-box-<?php echo $i++; ?>">
                            <div class="icon-play">
                                <img src="<?php the_field('wybierz_ikonę_linku_popup','options') ?>">
                            </div>
                            <img class="HomeRepeaterImg"
                                src="<?php echo $SingleServiceRepeaterImgPopup['sizes']['large']; ?>"
                                width="<?php echo $SingleServiceRepeaterImgPopup['sizes']['large']; ?>"
                                height="<?php echo $SingleServiceRepeaterImgPopup['sizes']['large']; ?>"
                                alt="<?php echo esc_attr($SingleServiceRepeaterImgPopup['alt']); ?>" />
                        </div>
                    </a>
                    <img class="img-apla-serv"
                        src="<?php the_field('tlo_pod_filmem_blok_services_single','options') ?>">
                    <?php  } ?>
                    <!-- end popup -->


                </div>
            </div>
        </div>
    </section>
    <?php } ?>
    <?php
                    endwhile;
                    else :
                    endif;
                    ?>

    <section class="realization">
        <div class="container">
            <div class="title-area">
                <h2><?php the_field('tytul_realizacje_single_services') ?></h2>
            </div>
            <div class="wrapper-realizations">
                <?php
$wybierz_kategorie = get_field('wybierz_kategorie_realizacji_realziation_in_services');
// $wybierz_kategorie = 'hotele';

$custom_query = new WP_Query( 
    array(
    'post_type' => 'realizacje',
     'post_status'=>'publish',
    'posts_per_page' => -1,

    'tax_query' => array(
        array(
          'taxonomy' => 'kategoria',
          'field'    => 'term_id',
          'terms'    => $wybierz_kategorie,
        )
      )

    ) 
);
?>


                <?php if ( $custom_query->have_posts() ) : while  ( $custom_query->have_posts() ) : $custom_query->the_post(); ?>
                <?php $realization_excerpt = get_the_excerpt($post->ID);?>

                <?php $urlImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' ); ?>

                <div class="realization-block-item">
                    <?php the_post_thumbnail('large'); ?>
                    <a class="block-link" href="<?php echo get_permalink(); ?>">
                        <h3><?php echo get_the_title(); ?></h3>
                        <div class="overly"></div>
                    </a>
                </div>



                <?php
endwhile; 
endif; 
wp_reset_query();
?>
            </div>

            <div class="btn-area">
                <a href="<?php the_field('przycisk_wiecej_realizacji_link_single_services');?>"
                    class="btn-grad btn-repeater bgWhite"><?php the_field('przycisk_wiecej_realizacji_tekst_single_services') ?></a>
            </div>

        </div>
    </section>

    <section class="cta-home" data-aos="fade-up" data-aos-duration="1000">
        <div class="cta-wrapper">
            <div class="container">
                <div class="title-area">
                    <h2><?php the_field('tytul_sekcja_z_kontaktem_Allepage','options') ?></h2>
                </div>
                <div class="text-area">
                    <?php the_field('tekst_sekcja_z_kontaktem_Allpage','options') ?>
                </div>
                <div class="btn-area">
                    <a href="<?php the_field('link_przycisku_sekcja_z_kontaktem_Allpage','options') ?>"
                        class="btn btn-grad"><?php the_field('tekst_przycisku_sekcja_z_kontaktem_Allpage','options') ?></a>
                </div>
            </div>
        </div>
    </section>
</main>

<?php get_footer();?>