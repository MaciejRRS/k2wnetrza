<?php
/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      https://searchandfilter.com
 * @copyright 2018 Search & Filter
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $query->have_posts() )
{
	?>




<?php
	while ($query->have_posts())
	{
		$query->the_post();
		
		?>


<div class="custom-post">

    <div class="post-area">

        <div class="post-thumbnail">
            <?php if ( has_post_thumbnail() ) {
                                    the_post_thumbnail('medium_large',true);
                                } ?>
        </div>

        <div class="post-text">
            <h2><?php echo mb_strimwidth( get_the_title(), 0, 50, '...' ); ?></h2>
            <p class="details_date"><?php echo get_the_date( $post_id ); ?> /
                <?php echo get_the_category( $id )[0]->name ; ?></p>
            <p><?php echo mb_strimwidth( get_the_excerpt(), 0, 300, '...' ); ?></p>
            <div class="details">


                <a href="<?php the_permalink(); ?>"><span class="btn-posts">zobacz więcej >></span> </a>
            </div>
        </div>


    </div>


</div>



<?php
	}
	?>


<!-- Page <?php echo $query->query['paged']; ?> of <?php echo $query->max_num_pages; ?><br /> -->

<div class="pagination">

    <div class="nav-previous"><?php next_posts_link( '<p>&laquo;  poprzednie</p>', $query->max_num_pages ); ?>
    </div>
    <div class="nav-next"><?php previous_posts_link( '<p>następne  &raquo;</p>' ); ?></div>
    <?php
			/* example code for using the wp_pagenavi plugin */
			if (function_exists('wp_pagenavi'))
			{
				echo "<br />";
				wp_pagenavi( array( 'query' => $query ) );
			}
		?>
</div>
<?php
}
else
{
	echo "Brak wyników";
}
?>