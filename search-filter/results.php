<?php
/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      https://searchandfilter.com
 * @copyright 2018 Search & Filter
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} ?>


<div class="container">
    <div class="wrapper-realizations">
        <?php if ( $query->have_posts() )
{
	?>

        <?php
	while ($query->have_posts())  
	{
		$query->the_post();
?>

        <?php if( get_field('wlacznik_sekcji_usługa_lista_real_single') ) { ?>
        <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' ); ?>

        <div class="realization-block-item">
            <!-- <img src="<?php // echo $backgroundImg[0]; ?>" alt=""> -->
            <?php the_post_thumbnail('large'); ?>
            <a class="block-link" href="<?php echo get_permalink(); ?>">
                <h3><?php echo get_the_title(); ?></h3>
                <div class="overly"></div>
            </a>
        </div>

        <?php }

	}
	?>
    </div>

</div>

<div class="container">
    <div class="pagination">
        <?php
    $nextBtn = get_field('przycisk_prawy_realization_all','options');
    $backBtn = get_field('przycisk_lewy_realization_all','options');

   
?>
        <div class="nav-next"><?php previous_posts_link( $backBtn  ) ?> </div>
        <div class="nav-previous"><?php next_posts_link(  $nextBtn, $query->max_num_pages ); ?>
        </div>

        <?php
        /* example code for using the wp_pagenavi plugin */
        if (function_exists('wp_pagenavi'))
        {
            echo "<br />";
            wp_pagenavi( array( 'query' => $query ) );
        }
    ?>
    </div>
</div>
<?php
}
else
{
	echo "<div class='noFound'>Brak realizacji w wybranej kategorii, wybierz inną kategorię</div>";
}
?>