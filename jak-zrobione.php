<?php 
/* 
Template Name: Jak to jest zrobione  
*/ 
?>

<?php get_header() ?>
<main id="howD0">

    <section class="hero hero-subpage">
        <div class="container">
            <div class="hero-wrapper">
                <div class="column-left" data-aos="fade-right" data-aos-duration="1000">
                    <h1><?php the_field('tytul_sekcja_1_howDo') ?></h1>
                    <?php the_field('tekst_sekcja_1_howDo') ?>
                </div>
                <div class="column-right" data-aos="fade-left" data-aos-duration="1000">
                    <?php $imageHeroHowDo = get_field('zdjecie_sekcja_1_howDo'); ?>
                    <img class="img-hero-homepage" src="<?php echo $imageHeroHowDo['sizes']['large']; ?>"
                        width="<?php echo $imageHeroHowDo['sizes']['large-width']; ?>"
                        height="<?php echo $imageHeroHowDo['sizes']['large-height']; ?>"
                        alt="<?php echo esc_attr($imageHeroHowDo['alt']); ?>" />
                </div>

            </div>
            <div class="breadcrumps">
                <?php if( function_exists( 'bcn_display' ) ) bcn_display(); ?>
            </div>
        </div>
        <a href="#jak-przebiega-wspolpraca-z-nami" class="arrow-icon-scroll">
            <div class="arrow-area">
                <?php the_field('scroll_napis','options') ?> <i class="arrow down"></i>
            </div>
        </a>
    </section>





    <section id="jak-przebiega-wspolpraca-z-nami" class="list-steps">
        <div class="container">
            <div class="title-area">
                <h2><?php the_field('tytuł_lista_krokow_howDo') ?></h2>
            </div>
            <?php
                    if( have_rows('repeater_lista_krokow_howDo') ): $i = 1;
                        while( have_rows('repeater_lista_krokow_howDo') ) : the_row(); ?>


            <div class="column-repeater" data-aos="fade-down">
                <h5 class="step"><?php the_sub_field('krok_text_lista_krokow_howDo'); ?>
                </h5>
                <div class="title-area">
                    <h3><?php the_sub_field('tytul_kroku_lista_krokow_howDo') ?></h3>
                </div>
                <div class="text-area">
                    <?php the_sub_field('opis_kroku_lista_krokow_howDo') ?>
                </div>
                <?php if( get_sub_field('imageListSteps') ): ?>
                <?php $imageListSteps = get_sub_field('imageListSteps'); ?>
                <img class="img-imageListSteps" src="<?php echo $imageListSteps['sizes']['large']; ?>"
                    width="<?php echo $imageListSteps['sizes']['large-width']; ?>"
                    height="<?php echo $imageListSteps['sizes']['large-height']; ?>"
                    alt="<?php echo esc_attr($imageListSteps['alt']); ?>" />
                <?php endif; ?>
            </div>
            <?php
                    endwhile;
                    else :
                    endif;
                    ?>
        </div>
    </section>




    <!-- start new popup single -->
    <?php
if( get_field('wlacz_dodatkowe_pole_popup_how') ) { ?>
    <section class="section-repeater section-repeater-<?php echo $i++; ?> change-side-realization">
        <div class="container">
            <div class="wrapper-repeater center-block">


                <div class="column-1">

                    <?php
                $spacer_zdjecie_single_realiz_custom_how = get_field('zdjecie_blok_repeater_single_realiz_custom_how'); 
                $spacer_film_single_realiz_custom_how = get_field('film_blok_repeater_single_realiz_custom_how'); 
                        
                $get_selection_single_realiz_custom_how = get_field('wybor_zdjecie_film_blok_repeater_single_realiz_custom_how'); 
                
                
                if ($get_selection_single_realiz_custom_how == 'zdjecie'){ ?>
                    <?php $RealSingleRepeaterImg_custom_how = get_field('zdjecie_blok_real_singl_custom_how'); ?>
                    <img class="HomeRepeaterImg"
                        src="<?php echo $RealSingleRepeaterImg_custom_how['sizes']['large']; ?>"
                        width="<?php echo $ServiceSingleRepeaterImg_custom_how['sizes']['large-width']; ?>"
                        height="<?php echo $RealSingleRepeaterImg_custom_how['sizes']['large-height']; ?>"
                        alt="<?php echo esc_attr($RealSingleRepeaterImg_custom_how['alt']); ?>" />

                    <?php  } else if ($get_selection_single_realiz_custom_how == 'film') { ?>
                    <div class="movie-area popup-area">
                        <?php the_field('film_blok_repeater_single_realiz_custom_how'); ?>
                    </div>
                    <img class="img-apla-serv"
                        src="<?php the_field('tlo_pod_filmem_blok_services_single','options') ?>">


                    <!-- start popup -->
                    <?php  } else if ($get_selection_single_realiz_custom_how == 'popup') { ?>
                    <!-- <div class="movie-area popup-area btn-noAnimate">
                        <?php $singleRealRepeaterImgPopup_custom_how = get_field('popup_blok_repeater_singleReal_custom_how'); ?>
                        <div id="myBtn" class="popup-clicable-area" data-toggle="modal" data-target="#modal-box-popup">
                            <div class="icon-play">
                                <img src="<?php the_field('wybierz_ikonę_linku_popup','options') ?>">
                            </div>
                            <img class="HomeRepeaterImg"
                                src="<?php echo $singleRealRepeaterImgPopup_custom_how['sizes']['large']; ?>"
                                width="<?php echo $singleRealRepeaterImgPopup_custom_how['sizes']['large-width']; ?>"
                                height="<?php echo $singleRealRepeaterImgPopup_custom_how['sizes']['large-height']; ?>"
                                alt="<?php echo esc_attr($singleRealRepeaterImgPopup_custom_how['alt']); ?>" />
                        </div>
                        <div id="modal-box-popup" class="modal">

                            <div id="mymodal-close" class="modal-content">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <iframe src="<?php the_field('popup_repeater_singleReal_content_custom_how'); ?>"
                                    frameborder="0"></iframe>
                            </div>

                        </div>

                    </div> -->

                    <a href="<?php the_field('popup_repeater_singleReal_content_custom_how'); ?>" target="_blank"
                        class="movie-area popup-area btn-noAnimate">
                        <?php $singleRealRepeaterImgPopup_custom_how = get_field('popup_blok_repeater_singleReal_custom_how'); ?>
                        <div id="myBtn" class="popup-clicable-area" data-toggle="modal" data-target="#modal-box-popup">
                            <div class="icon-play">
                                <img src="<?php the_field('wybierz_ikonę_linku_popup','options') ?>">
                            </div>
                            <img class="HomeRepeaterImg"
                                src="<?php echo $singleRealRepeaterImgPopup_custom_how['sizes']['large']; ?>"
                                width="<?php echo $singleRealRepeaterImgPopup_custom_how['sizes']['large-width']; ?>"
                                height="<?php echo $singleRealRepeaterImgPopup_custom_how['sizes']['large-height']; ?>"
                                alt="<?php echo esc_attr($singleRealRepeaterImgPopup_custom_how['alt']); ?>" />
                        </div>
                    </a>
                    <img class="img-apla-serv"
                        src="<?php the_field('tlo_pod_filmem_blok_services_single','options') ?>">
                    <?php  } ?>
                    <!-- end popup -->


                </div>
            </div>

        </div>
    </section>
    <?php } ?>
    <!-- end popup single -->




    <section class="benefits">
        <div class="container">
            <div class="title-area">
                <h2><?php the_field('tytul_sekcji_lista_blokow_z_ikona_howDo') ?></h2>
            </div>
            <div class="text-area">
                <?php the_field('tresc_sekcji_lista_blokow_z_ikona_howDo') ?>
            </div>

            <div class="block-area-repeater-icons">
                <?php
                    if( have_rows('lista_blokow_z_ikona_howDo') ): $i = 2;
                        while( have_rows('lista_blokow_z_ikona_howDo') ) : the_row(); ?>
                <div class="blok-item" data-aos="fade-down" data-aos-duration="1000">
                    <div class="icon-item">
                        <?php $imgHowDo_list_icon = get_sub_field('ikona_lista_blokow_z_ikona_howDo'); ?>
                        <img class="RepeaterImgIcon" src="<?php echo $imgHowDo_list_icon['sizes']['large']; ?>"
                            width="<?php echo $imgHowDo_list_icon['sizes']['large-width']; ?>"
                            height="<?php echo $imgHowDo_list_icon['sizes']['large-height']; ?>"
                            alt="<?php echo esc_attr($imgHowDo_list_icon['alt']); ?>" />
                    </div>
                    <div class="text-block-item">
                        <h3><?php the_sub_field('tytul_lista_blokow_z_ikona_howDo') ?></h3>
                        <?php the_sub_field('tresc_lista_blokow_z_ikona_howDo') ?>
                    </div>
                </div>
                <?php
                    endwhile;
                    else :
                    endif;
                    ?>
            </div>
        </div>
    </section>



    <?php if( get_field('title_repeater_all_sections_howDo') ): ?>
    <div class="title-area-for-repeater">
        <div class="container">
            <div class="title-area">
                <h2><?php the_field('title_repeater_all_sections_howDo') ?></h2>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <!-- wyłaczane sekcje -->
    <?php
        if( have_rows('lista_blokow_ze_zdjeciem_howDo') ): $i = 2;
        while( have_rows('lista_blokow_ze_zdjeciem_howDo') ) : the_row(); ?>

    <?php
if( get_sub_field('wlacz__wylacz_lista_blokow_ze_zdjeciem_howDo') ) { ?>
    <section class="section-repeater change-side-realization sec-<?php echo $i++; ?>">
        <div class="container">
            <div class="wrapper-repeater" data-aos="fade-up" data-aos-duration="1000">
                <div class="column-2">
                    <div class="title-area">
                        <h2><?php the_sub_field('tytul_lista_blokow_ze_zdjeciem_howDo') ?></h2>
                    </div>
                    <div class="text-area">
                        <?php the_sub_field('tresc_lista_blokow_ze_zdjeciem_howDo') ?>
                    </div>
                    <div class="btn-area">
                        <?php if( get_sub_field('przycisk1_tekst_lista_blokow_ze_zdjeciem_howDo') ): ?>
                        <a href="<?php the_sub_field('przycisk1_link_lista_blokow_ze_zdjeciem_howDo') ?>"
                            class="btn btn-grad btn-repeater"><?php the_sub_field('przycisk1_tekst_lista_blokow_ze_zdjeciem_howDo') ?></a>
                        <?php endif; ?>

                        <?php if( get_sub_field('przycisk2_tekst_lista_blokow_ze_zdjeciem_howDo') ): ?>
                        <a href="<?php the_sub_field('przycisk2_link_lista_blokow_ze_zdjeciem_howDo');?>"
                            class="btn-grad btn-repeater bgWhite"><?php the_sub_field('przycisk2_tekst_lista_blokow_ze_zdjeciem_howDo') ?></a>
                        <?php endif; ?>

                    </div>
                </div>
                <div class="column-1">

                    <?php
                $spacer_zdjecie_howDo_list = get_sub_field('zdjecie_blok_repeater_howDo_list'); 
                $spacer_film_howDo_list = get_sub_field('film_blok_repeater_howDo_list'); 
                $get_selection_howDo_list = get_sub_field('wybor_zdjecie_film_blok_repeater_howDo_list'); 
                
                if ($get_selection_howDo_list == 'zdjecie'){ ?>
                    <?php $imgHowDo_list = get_sub_field('zdjecie_link_lista_blokow_ze_zdjeciem_howDo'); ?>
                    <img class="HomeRepeaterImg" src="<?php echo $imgHowDo_list['sizes']['large']; ?>"
                        width="<?php echo $imgHowDo_list['sizes']['large-width']; ?>"
                        height="<?php echo $imgHowDo_list['sizes']['large-height']; ?>"
                        alt="<?php echo esc_attr($imgHowDo_list['alt']); ?>" />
                    <?php  } else if ($get_selection_howDo_list == 'film') { ?>
                    <div class="movie-area popup-area">
                        <?php the_sub_field('film_blok_repeater_howDo_list'); ?>
                    </div>
                    <img class="img-apla-serv"
                        src="<?php the_field('tlo_pod_filmem_blok_services_single','options') ?>">


                    <!-- start popup -->
                    <?php  } else if ($get_selection_howDo_list == 'popup') { ?>
                    <div class="movie-area popup-area btn-noAnimate">
                        <?php $HowDoRepeaterImgPopup = get_sub_field('popup_blok_repeater_HowDo'); ?>
                        <div id="myBtn" class="popup-clicable-area" data-toggle="modal"
                            data-target="#modal-box-<?php echo $i++; ?>">
                            <div class="icon-play">
                                <img src="<?php the_field('wybierz_ikonę_linku_popup','options') ?>">
                            </div>
                            <img class="HomeRepeaterImg" src="<?php echo $HowDoRepeaterImgPopup['sizes']['large']; ?>"
                                width="<?php echo $HowDoRepeaterImgPopup['sizes']['large-width']; ?>"
                                height="<?php echo $HowDoRepeaterImgPopup['sizes']['large-height']; ?>"
                                alt="<?php echo esc_attr($HowDoRepeaterImgPopup['alt']); ?>" />
                        </div>
                        <div id="modal-box-<?php echo --$i; ?>" class="modal">

                            <!-- Modal content -->
                            <div id="mymodal-close" class="modal-content">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <iframe src="<?php the_sub_field('popup_repeater_HowDo_content'); ?>"
                                    frameborder="0"></iframe>
                            </div>

                        </div>

                    </div>
                    <img class="img-apla-serv"
                        src="<?php the_field('tlo_pod_filmem_blok_services_single','options') ?>">
                    <?php  } ?>
                    <!-- end popup -->


                </div>
            </div>

        </div>
    </section>
    <?php } ?>

    <?php
                    endwhile;
                    else :
                    endif;
                    ?>
    <!-- end wyłaczane sekcje -->





    <section class="cta-home" data-aos="fade-up" data-aos-duration="1000">
        <div class="cta-wrapper">
            <div class="container">
                <div class="title-area">
                    <h2><?php the_field('tytul_sekcja_z_kontaktem_Allepage','options') ?></h2>
                </div>
                <div class="text-area">
                    <?php the_field('tekst_sekcja_z_kontaktem_Allpage','options') ?>
                </div>
                <div class="btn-area">
                    <a href="<?php the_field('link_przycisku_sekcja_z_kontaktem_Allpage','options') ?>"
                        class="btn btn-grad"><?php the_field('tekst_przycisku_sekcja_z_kontaktem_Allpage','options') ?></a>
                </div>
            </div>
        </div>
    </section>

</main>
















<?php get_footer() ?>