<?php get_header() ;?>


<?php $realization_excerpt = get_the_excerpt($post->ID);?>

<main id="services">
    <section class="hero hero-subpage">
        <div class="container">
            <div class="hero-wrapper">
                <div class="column-left" data-aos="fade-right" data-aos-duration="1000">
                    <h1><?php the_title(); ?></h1>
                    <?php echo '<p>'.$realization_excerpt.'</p>' ?>
                    <div class="buttons-wrapper">
                        <?php if( get_field('przycisk_tekst_zapytaj_o_oferte_real_single') ): ?>
                        <a href="<?php the_field('przycisk_link_zapytaj_o_oferte_real_single') ?>"
                            class="btn btn-grad"><?php the_field('przycisk_tekst_zapytaj_o_oferte_real_single') ?></a>
                        <?php endif; ?>

                        <?php if( get_field('przycisk_tekst_sprawdz_realizacje_real_single') ): ?>
                        <a href="<?php the_field('przycisk_link_sprawdz_realizacje_real_single');?>"
                            class="btn-grad bgWhite"><?php the_field('przycisk_tekst_sprawdz_realizacje_real_single') ?></a>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="column-right wrapper-repeater" data-aos="fade-left" data-aos-duration="1000">

                    <?php

                $spacer_film_single_realiz_custom_first = get_field('film_blok_repeater_single_realiz_custom_first'); 
                $get_selection_single_realiz_custom_first = get_field('wybor_zdjecie_film_blok_repeater_single_realiization_hero'); 
                
                
                if ($get_selection_single_realiz_custom_first == 'zdjecie'){ ?>
                    <?php $RealSingleRepeaterImg_custom = get_field('zdjecie_sekcja_1_single_real_single'); ?>
                    <img class="img-hero-homepage" src="<?php echo $RealSingleRepeaterImg_custom['sizes']['large']; ?>"
                        width="<?php echo $ServiceSingleRepeaterImg_custom['sizes']['large']; ?>"
                        height="<?php echo $RealSingleRepeaterImg_custom['sizes']['large']; ?>"
                        alt="<?php echo esc_attr($RealSingleRepeaterImg_custom['alt']); ?>" />

                    <?php  } else if ($get_selection_single_realiz_custom_first == 'film') { ?>
                    <div class="movie-area popup-area">
                        <?php the_field('film_blok_repeater_single_realiz_custom'); ?>
                    </div>

                    <!-- start popup -->
                    <?php  } else if ($get_selection_single_realiz_custom_first == 'popup') { ?>
                    <!-- <div class="movie-area popup-area btn-noAnimate">
                        <?php $singleRealRepeaterImgPopup_custom = get_field('popup_blok_repeater_singleReal_custom'); ?>
                        <div id="myBtn" class="popup-clicable-area" data-toggle="modal" data-target="#modal-box-first">
                            <div class="icon-play">
                                <img src="<?php the_field('wybierz_ikonę_linku_popup','options') ?>">
                            </div>
                            <img class="img-hero-homepage"
                                src="<?php echo $singleRealRepeaterImgPopup_custom['sizes']['large']; ?>"
                                width="<?php echo $singleRealRepeaterImgPopup_custom['sizes']['large']; ?>"
                                height="<?php echo $singleRealRepeaterImgPopup_custom['sizes']['large']; ?>"
                                alt="<?php echo esc_attr($singleRealRepeaterImgPopup_custom['alt']); ?>" />
                        </div>
                        <div id="modal-box-first" class="modal">

                            
                            <div id="mymodal-close" class="modal-content">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <iframe src="<?php the_field('popup_repeater_singleReal_content_custom'); ?>"
                                    frameborder="0"></iframe>
                            </div>

                        </div>

                    </div> -->


                    <a href="<?php the_field('popup_repeater_singleReal_content_custom'); ?>" target="_blank"
                        class="movie-area popup-area btn-noAnimate">
                        <?php $singleRealRepeaterImgPopup_custom = get_field('popup_blok_repeater_singleReal_custom'); ?>
                        <div id="myBtn" class="popup-clicable-area" data-toggle="modal" data-target="#modal-box-first">
                            <div class="icon-play">
                                <img src="<?php the_field('wybierz_ikonę_linku_popup','options') ?>">
                            </div>
                            <img class="img-hero-homepage"
                                src="<?php echo $singleRealRepeaterImgPopup_custom['sizes']['large']; ?>"
                                width="<?php echo $singleRealRepeaterImgPopup_custom['sizes']['large']; ?>"
                                height="<?php echo $singleRealRepeaterImgPopup_custom['sizes']['large']; ?>"
                                alt="<?php echo esc_attr($singleRealRepeaterImgPopup_custom['alt']); ?>" />
                        </div>

                    </a> <?php  } ?>
                    <!-- end popup -->


                </div>
            </div>
            <div class="breadcrumps">
                <?php if( function_exists( 'bcn_display' ) ) bcn_display(); ?>
            </div>
        </div>
        <a href="#zobacz-realizacje" class="arrow-icon-scroll">
            <div class="arrow-area">
                <?php the_field('scroll_napis','options') ?> <i class="arrow down"></i>
            </div>
        </a>
    </section>


    <section id="zobacz-realizacje" class="section_no_image" data-aos="fade-up" data-aos-duration="1000">
        <div class="section_no_image_wrapper">
            <div class="container">
                <div class="title-area">
                    <h2><?php the_field('tytul_sekcja_bez_zdjecia_real_singl') ?></h2>
                </div>
                <?php if( get_field('tekst_sekcja_bez_zdjecia_real_singl') ): ?>
                <div class="text-area">
                    <?php the_field('tekst_sekcja_bez_zdjecia_real_singl') ?>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </section>




    <!-- start new popup single -->

    <section class="section-repeater section-repeater-<?php echo $i++; ?> change-side-realization">
        <div class="container">
            <div class="wrapper-repeater center-block">


                <div class="column-1">

                    <?php
                $spacer_zdjecie_single_realiz_custom = get_field('zdjecie_blok_repeater_single_realiz_custom'); 
                $spacer_film_single_realiz_custom = get_field('film_blok_repeater_single_realiz_custom'); 
                        
                $get_selection_single_realiz_custom = get_field('wybor_zdjecie_film_blok_repeater_single_realiz_custom'); 
                
                
                if ($get_selection_single_realiz_custom == 'zdjecie'){ ?>
                    <?php $RealSingleRepeaterImg_custom = get_field('zdjecie_blok_real_singl_custom'); ?>
                    <img class="HomeRepeaterImg" src="<?php echo $RealSingleRepeaterImg_custom['sizes']['large']; ?>"
                        width="<?php echo $ServiceSingleRepeaterImg_custom['sizes']['large']; ?>"
                        height="<?php echo $RealSingleRepeaterImg_custom['sizes']['large']; ?>"
                        alt="<?php echo esc_attr($RealSingleRepeaterImg_custom['alt']); ?>" />

                    <?php  } else if ($get_selection_single_realiz_custom == 'film') { ?>
                    <div class="movie-area popup-area">
                        <?php the_field('film_blok_repeater_single_realiz_custom'); ?>
                    </div>
                    <!-- <img class="img-apla-serv"
                        src="<?php the_field('tlo_pod_filmem_blok_services_single','options') ?>"> -->


                    <!-- start popup -->
                    <?php  } else if ($get_selection_single_realiz_custom == 'popup') { ?>
                    <!-- <div class="movie-area popup-area btn-noAnimate">
                        <?php $singleRealRepeaterImgPopup_custom = get_field('popup_blok_repeater_singleReal_custom'); ?>
                        <div id="myBtn" class="popup-clicable-area" data-toggle="modal" data-target="#modal-box-popup">
                            <div class="icon-play">
                                <img src="<?php the_field('wybierz_ikonę_linku_popup','options') ?>">
                            </div>
                            <img class="HomeRepeaterImg"
                                src="<?php echo $singleRealRepeaterImgPopup_custom['sizes']['large']; ?>"
                                width="<?php echo $singleRealRepeaterImgPopup_custom['sizes']['large']; ?>"
                                height="<?php echo $singleRealRepeaterImgPopup_custom['sizes']['large']; ?>"
                                alt="<?php echo esc_attr($singleRealRepeaterImgPopup_custom['alt']); ?>" />
                        </div>
                        <div id="modal-box-popup" class="modal">

                            
                            <div id="mymodal-close" class="modal-content">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <iframe src="<?php the_field('popup_repeater_singleReal_content_custom'); ?>"
                                    frameborder="0"></iframe>
                            </div>

                        </div>

                    </div> -->

                    <a href="<?php the_field('popup_repeater_singleReal_content_custom'); ?>" target="_blank"
                        class="movie-area popup-area btn-noAnimate">
                        <?php $singleRealRepeaterImgPopup_custom = get_field('popup_blok_repeater_singleReal_custom'); ?>
                        <div id="myBtn" class="popup-clicable-area" data-toggle="modal" data-target="#modal-box-popup">
                            <div class="icon-play">
                                <img src="<?php the_field('wybierz_ikonę_linku_popup','options') ?>">
                            </div>
                            <img class="HomeRepeaterImg"
                                src="<?php echo $singleRealRepeaterImgPopup_custom['sizes']['large']; ?>"
                                width="<?php echo $singleRealRepeaterImgPopup_custom['sizes']['large']; ?>"
                                height="<?php echo $singleRealRepeaterImgPopup_custom['sizes']['large']; ?>"
                                alt="<?php echo esc_attr($singleRealRepeaterImgPopup_custom['alt']); ?>" />
                        </div>
                    </a>

                    <?php  } ?>
                    <!-- end popup -->


                </div>
            </div>

        </div>
    </section>

    <!-- end popup single -->



















    <?php if( get_field('pokaz_ukryj_dodatkowej_sekcji_seo_k2_realizacje') ): ?>
    <section class="section_no_image no-padding-top" data-aos="fade-up" data-aos-duration="1000">
        <div class="section_no_image_wrapper">
            <div class="container">
                <div class="title-area">
                    <h2><?php the_field('naglowek_dodatkowej_sekcji_seo_k2_realizacje') ?></h2>
                </div>
                <div class="text-area">
                    <?php the_field('tekst_dodatkowej_sekcji_seo_k2_realizacje') ?>
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>















    <?php if( get_field('title_for_sections_list_repeater_in_single_realization') ): ?>
    <div class="sections-title" data-aos="fade-down" data-aos-duration="1000">
        <div class="title-area">
            <div class="container">
                <h2><?php the_field('title_for_sections_list_repeater_in_single_realization'); ?></h2>
            </div>
        </div>
    </div>
    <?php endif; ?>


    <?php
                    if( have_rows('lista_blokow_repeater_real_singl') ): $i = 2;
                        while( have_rows('lista_blokow_repeater_real_singl') ) : the_row(); ?>

    <?php
if( get_sub_field('wlaczwylacz_blok_realizacja_lista_repeater') ) { ?>

    <section class="section-repeater section-repeater-<?php echo $i++; ?> change-side-realization">
        <div class="container">
            <div class="wrapper-repeater" data-aos="fade-up" data-aos-duration="1000">

                <div class="column-2">
                    <div class="title-area">
                        <h2><?php the_sub_field('tytul_blok_repeater_real_single') ?></h2>
                    </div>
                    <div class="text-area">
                        <?php the_sub_field('tekst_blok_repeater_real_single') ?>
                    </div>

                    <div class="btn-area">
                        <?php if( get_sub_field('przycisk__sprawdz_oferte_tekst_poj_realizacja') ): ?>
                        <a href="<?php the_sub_field('przycisk__sprawdz_oferte_link_poj_realizacja') ?>"
                            class="btn btn-grad btn-repeater"><?php the_sub_field('przycisk__sprawdz_oferte_tekst_poj_realizacja') ?></a>
                        <?php endif; ?>

                        <?php if( get_sub_field('przycisk_sprawdz_realizacje_tekst_poj_realizacja') ): ?>
                        <a href="<?php the_sub_field('przycisk_sprawdz_realizacje_link_poj_realizacja');?>"
                            class="btn-grad btn-repeater bgWhite"><?php the_sub_field('przycisk_sprawdz_realizacje_tekst_poj_realizacja') ?></a>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="column-1">

                    <?php
                $spacer_zdjecie_single_realiz = get_sub_field('zdjecie_blok_repeater_single_realiz'); 
                $spacer_film_single_realiz = get_sub_field('film_blok_repeater_single_realiz'); 
                        
                $get_selection_single_realiz = get_sub_field('wybor_zdjecie_film_blok_repeater_single_realiz'); 
                
                
                if ($get_selection_single_realiz == 'zdjecie'){ ?>
                    <?php $RealSingleRepeaterImg = get_sub_field('zdjecie_blok_real_singl'); ?>
                    <img class="HomeRepeaterImg" src="<?php echo $RealSingleRepeaterImg['sizes']['large']; ?>"
                        width="<?php echo $ServiceSingleRepeaterImg['sizes']['large']; ?>"
                        height="<?php echo $RealSingleRepeaterImg['sizes']['large']; ?>"
                        alt="<?php echo esc_attr($RealSingleRepeaterImg['alt']); ?>" />
                    <?php  } else if ($get_selection_single_realiz == 'film') { ?>

                    <div class="movie-area popup-area">
                        <?php the_sub_field('film_blok_repeater_single_realiz'); ?>
                    </div>
                    <img class="img-apla-serv"
                        src="<?php the_field('tlo_pod_filmem_blok_services_single','options') ?>">


                    <!-- start popup -->
                    <?php  } else if ($get_selection_single_realiz == 'popup') { ?>
                    <div class="movie-area popup-area btn-noAnimate">
                        <?php $singleRealRepeaterImgPopup = get_sub_field('popup_blok_repeater_singleReal'); ?>
                        <div id="myBtn" class="popup-clicable-area" data-toggle="modal"
                            data-target="#modal-box-<?php echo $i++; ?>">
                            <div class="icon-play">
                                <img src="<?php the_field('wybierz_ikonę_linku_popup','options') ?>">
                            </div>
                            <img class="HomeRepeaterImg"
                                src="<?php echo $singleRealRepeaterImgPopup['sizes']['large']; ?>"
                                width="<?php echo $singleRealRepeaterImgPopup['sizes']['large']; ?>"
                                height="<?php echo $singleRealRepeaterImgPopup['sizes']['large']; ?>"
                                alt="<?php echo esc_attr($singleRealRepeaterImgPopup['alt']); ?>" />
                        </div>
                        <div id="modal-box-<?php echo --$i; ?>" class="modal">

                            <!-- Modal content -->
                            <div id="mymodal-close" class="modal-content">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <iframe src="<?php the_sub_field('popup_repeater_singleReal_content'); ?>"
                                    frameborder="0"></iframe>
                            </div>

                        </div>

                    </div>
                    <img class="img-apla-serv"
                        src="<?php the_field('tlo_pod_filmem_blok_services_single','options') ?>">
                    <?php  } ?>
                    <!-- end popup -->


                </div>
            </div>

        </div>
    </section>
    <?php } ?>

    <?php
                    endwhile;
                    else :
                    endif;
                    ?>








    <section class="realization">
        <div class="container">
            <div class="title-area">
                <h2><?php the_field('tytul_realizacje_single_services_rrs') ?></h2>
            </div>
            <div class="wrapper-realizations">
                <?php
$wybierz_kategorie = get_field('wybierz_kategorie_realizacji_do_wyswietlenia_rrs');
// $wybierz_kategorie = 'hotele';

$custom_query = new WP_Query( 
    array(
    'post_type' => 'realizacje',
     'post_status'=>'publish',
    'posts_per_page' => 6,

    'tax_query' => array(
        array(
          'taxonomy' => 'kategoria',
          'field'    => 'term_id',
          'terms'    => $wybierz_kategorie,
        )
      )

    ) 
);
?>


                <?php if ( $custom_query->have_posts() ) : while  ( $custom_query->have_posts() ) : $custom_query->the_post(); ?>
                <?php $realization_excerpt = get_the_excerpt($post->ID);?>
                <?php $urlImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' ); ?>



                <div class="realization-block-item">
                    <img src="<?php echo $urlImg[0]; ?>">
                    <a class="block-link" href="<?php echo get_permalink(); ?>">
                        <h3><?php echo get_the_title(); ?></h3>
                        <div class="overly"></div>
                    </a>
                </div>


                <?php
endwhile; 
endif; 
wp_reset_query();
?>
            </div>
            <?php if( get_field('przycisk_wiecej_realizacji_tekst_single_services_rrs') ): ?>
            <div class="btn-area">
                <a href="<?php the_field('przycisk_wiecej_realizacji_link_single_services_rrs');?>"
                    class="btn-grad btn-repeater bgWhite"><?php the_field('przycisk_wiecej_realizacji_tekst_single_services_rrs') ?></a>
            </div>
            <?php endif; ?>
        </div>
    </section>











    <section class="cta-home" data-aos="fade-up" data-aos-duration="1000">
        <div class="cta-wrapper">
            <div class="container">
                <div class="title-area">
                    <h2><?php the_field('tytul_sekcja_z_kontaktem_Allepage','options') ?></h2>
                </div>
                <div class="text-area">
                    <?php the_field('tekst_sekcja_z_kontaktem_Allpage','options') ?>
                </div>
                <div class="btn-area">
                    <a href="<?php the_field('link_przycisku_sekcja_z_kontaktem_Allpage','options') ?>"
                        class="btn btn-grad"><?php the_field('tekst_przycisku_sekcja_z_kontaktem_Allpage','options') ?></a>
                </div>
            </div>
        </div>
    </section>
</main>

<?php get_footer();?>