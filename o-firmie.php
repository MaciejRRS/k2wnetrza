<?php 
/* 
Template Name: O firmie  
*/ 
?>

<?php get_header() ?>
<main id="about">
    <section class="hero hero-subpage">
        <div class="container">
            <div class="hero-wrapper">
                <div class="column-left" data-aos="fade-right" data-aos-duration="1000">
                    <h1><?php the_field('tytul_hero_description_about');?></h1>
                    <?php the_field('tekst_hero_description_about') ?>
                    <div class="buttons-wrapper">
                        <?php if( get_field('przycisk_zapytaj_o_oferte_tekst_about') ): ?>
                        <a href="<?php the_field('przycisk_zapytaj_o_oferte_link_about') ?>"
                            class="btn btn-grad"><?php the_field('przycisk_zapytaj_o_oferte_tekst_about') ?></a>
                        <?php endif; ?>
                        <?php if( get_field('przycisk_sprawdz_realizacje_tekst_about') ): ?>
                        <a href="<?php the_field('przycisk_sprawdz_realizacje_link');?>"
                            class="btn-grad bgWhite"><?php the_field('przycisk_sprawdz_realizacje_tekst_about') ?></a>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="column-right" data-aos="fade-left" data-aos-duration="1000">
                    <?php $imageHeroAbout = get_field('zdjecie_sekcja_1_about_img'); ?>
                    <img class="img-hero-homepage" src="<?php echo $imageHeroAbout['sizes']['large']; ?>"
                        width="<?php echo $imageHeroAbout['sizes']['large-width']; ?>"
                        height="<?php echo $imageHeroAbout['sizes']['large-height']; ?>"
                        alt="<?php echo esc_attr($imageHeroAbout['alt']); ?>" />

                </div>
            </div>
            <div class="breadcrumps">
                <?php if( function_exists( 'bcn_display' ) ) bcn_display(); ?>
            </div>
        </div>

        <a href="#jak-to-sie-zaczelo" class="arrow-icon-scroll">
            <div class="arrow-area">
                <?php the_field('scroll_napis','options') ?> <i class="arrow down"></i>
            </div>
        </a>
    </section>


    <section id="jak-to-sie-zaczelo" class="section_no_image section-repeater section-repeater-2" data-aos="fade-down"
        data-aos-duration="1000">
        <div class="section_no_image_wrapper">
            <div class="container">
                <div class="title-area">
                    <h2><?php the_field('tytul_sekcja_bez_zdjecia_about') ?></h2>
                </div>
                <div class="text-area">
                    <?php the_field('tekst_sekcja_bez_zdjecia_about') ?>
                </div>
            </div>
        </div>
    </section>




    <?php
                    if( have_rows('lista_blokow_repeater_about') ): $i = 3;
                        while( have_rows('lista_blokow_repeater_about') ) : the_row(); ?>
    <section class="section-repeater section-repeater-<?php echo $i++; ?> change-side">
        <div class="container">
            <div class="wrapper-repeater">

                <div class="column-2" data-aos="fade-right" data-aos-duration="1000">
                    <div class="title-area">
                        <h2><?php the_sub_field('tytul_blok_repeater_about') ?></h2>
                    </div>
                    <div class="text-area">
                        <?php the_sub_field('tekst_blok_repeater_about') ?>
                    </div>

                    <div class="social-media-wrapper">
                        <a href="<?php the_sub_field('icona_social_media_fb_about_link') ?>"><img
                                src="<?php the_sub_field('icona_social_media_fb_about') ?>" class="smImg"></a>
                        <a href="<?php the_sub_field('icona_social_media_indeed_about_link') ?>"><img
                                src="<?php the_sub_field('icona_social_media_indeed_about') ?>" class="smImg"></a>
                        <a href="<?php the_sub_field('icona_social_media_instagram_about_link') ?>"><img
                                src="<?php the_sub_field('icona_social_media_instagram_about') ?>" class="smImg"></a>
                    </div>
                </div>
                <div class="column-1" data-aos="fade-left" data-aos-duration="1000">
                    <?php $AboutSingleRepeaterImg = get_sub_field('zdjecie_blok_about'); ?>
                    <img class="HomeRepeaterImg" src="<?php echo $AboutSingleRepeaterImg['sizes']['large']; ?>"
                        width="<?php echo $AboutSingleRepeaterImg['sizes']['large-width']; ?>"
                        height="<?php echo $AboutSingleRepeaterImg['sizes']['large-height']; ?>"
                        alt="<?php echo esc_attr($AboutSingleRepeaterImg['alt']); ?>" />
                </div>
            </div>

        </div>
    </section>
    <?php
                    endwhile;
                    else :
                    endif;
                    ?>






    <section class="comments">
        <div class="container">

            <div class="title-area">
                <h2><?php the_field('tytul_sekcji_komentarze_about') ?></h2>
            </div>
        </div>
        <!-- Swiper -->
        <div class="appla-swiper-about">
            <div class="container">
                <div class="container-about-slider">
                    <div class="swiper mySwiper">
                        <div class="swiper-wrapper">
                            <?php
                    if( have_rows('lista_komentarzy_about') ):
                        while( have_rows('lista_komentarzy_about') ) : the_row(); ?>
                            <div class="swiper-slide">
                                <div class="comment-wrapper">
                                    <div class="column-left">


                                        <?php $AboutComentsImg = get_sub_field('zdjecie_komentujacego_uzytkownika_about'); ?>
                                        <img class="img-comment-user"
                                            src="<?php echo $AboutComentsImg['sizes']['medium']; ?>"
                                            width="<?php echo $AboutComentsImg['sizes']['medium-width']; ?>"
                                            height="<?php echo $AboutComentsImg['sizes']['medium-height']; ?>"
                                            alt="<?php echo esc_attr($AboutComentsImg['alt']); ?>" />





                                    </div>
                                    <div class="column-right">
                                        <div class="comment-text-area">
                                            <h4><?php the_sub_field('tytul_komentarza_about') ?></h4>
                                            <?php the_sub_field('tresc_komentarza_about') ?>
                                        </div>
                                        <div class="user-info-area">
                                            <p class="user-name"><?php the_sub_field('username_about') ?></p>
                                            <span
                                                class="ranga-user"><?php the_sub_field('stanowisko_user_about') ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                    endwhile;
                    else :
                    endif;
                    ?>

                        </div>

                    </div>

                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>
            </div>
        </div>
    </section>





    <section class="section_no_image" data-aos="fade-down" data-aos-duration="1000">
        <div class="section_no_image_wrapper">
            <div class="container">
                <div class="title-area">
                    <h2><?php the_field('tytul_sekcja_bez_zdjecia_bottom') ?></h2>
                </div>
                <div class="text-area">
                    <?php the_field('tekst_sekcja_bez_zdjecia_about_bottom') ?>
                </div>
            </div>
        </div>
    </section>



    <section class="partners" data-aos="fade-down" data-aos-duration="1000">
        <div class="container">
            <div class="box-partners">
                <h3><?php the_field('tytul_sekcji_z_partnerami_sekcja_1_about') ?></h3>


                <div class="partners-list-static">
                    <!-- partners > 991px -->
                    <?php
                    if( have_rows('lista_partnerow_sekcja_1_about') ):
                        while( have_rows('lista_partnerow_sekcja_1_about') ) : the_row(); ?>
                    <div class="img-item-wrapper">
                        <?php $AboutPartnerlogo = get_sub_field('logo_partnera_sekcja_1_about'); ?>
                        <img class="img" src="<?php echo $AboutPartnerlogo['sizes']['thumbnail']; ?>"
                            width="<?php echo $AboutPartnerlogo['sizes']['thumbnail-width']; ?>"
                            height="<?php echo $AboutPartnerlogo['sizes']['thumbnail-height']; ?>"
                            alt="<?php echo esc_attr($AboutPartnerlogo['alt']); ?>" />
                    </div>
                    <?php
                    endwhile;
                    else :
                    endif;
                    ?>

                    <!-- end partners > 991px -->
                </div>







            </div>

        </div>
    </section>







    <?php
                    if( have_rows('lista_blokow_repeater_aboutPage_single') ): $i = 4;
                        while( have_rows('lista_blokow_repeater_aboutPage_single') ) : the_row(); ?>


    <?php if( get_sub_field('wlaczwylacz_blok_rep_aboutPage_single_block') ) { ?>
    <section class="section-repeater section-repeater-<?php echo $i++; ?> nth-about-block">
        <div class="container">
            <div class="wrapper-repeater">
                <div class="column-1" data-aos="fade-right" data-aos-duration="1000">
                    <?php $aboutPageSingleRepeaterImg = get_sub_field('zdjecie_blok_aboutPage_single'); ?>
                    <img class="HomeRepeaterImg" src="<?php echo $aboutPageSingleRepeaterImg['sizes']['large']; ?>"
                        width="<?php echo $aboutPageSingleRepeaterImg['sizes']['large']; ?>"
                        height="<?php echo $aboutPageSingleRepeaterImg['sizes']['large']; ?>"
                        alt="<?php echo esc_attr($aboutPageSingleRepeaterImg['alt']); ?>" />
                </div>
                <div class="column-2" data-aos="fade-left" data-aos-duration="1000">

                    <div class="title-area">
                        <h2><?php the_sub_field('tytul_blok_repeater_aboutPage_single') ?></h2>
                    </div>
                    <div class="text-area">
                        <?php the_sub_field('tekst_blok_repeater_aboutPage_single') ?>
                    </div>

                    <div class="btn-area">
                        <a href="<?php the_sub_field('przycisk_link_aboutPage_lista_oferta');?>"
                            class="btn-grad btn-repeater"><?php the_sub_field('przycisk_tekst_aboutPage_lista_oferta') ?></a>
                        <a href="<?php the_sub_field('przycisk_link_aboutPage_lista_realizacja');?>"
                            class="btn-grad btn-repeater bgWhite"><?php the_sub_field('przycisk_tekst_aboutPage_lista_realizacja') ?></a>
                    </div>
                </div>

            </div>

        </div>
    </section>
    <?php } ?>
    <?php
                    endwhile;
                    else :
                    endif;
                    ?>





    <section class="cta-home" data-aos="fade-down" data-aos-duration="1000">
        <div class="cta-wrapper">
            <div class="container">
                <div class="title-area">
                    <h2><?php the_field('tytul_sekcja_z_kontaktem_Allepage','options') ?></h2>
                </div>
                <div class="text-area">
                    <?php the_field('tekst_sekcja_z_kontaktem_Allpage','options') ?>
                </div>
                <div class="btn-area">
                    <a href="<?php the_field('link_przycisku_sekcja_z_kontaktem_Allpage','options') ?>"
                        class="btn btn-grad"><?php the_field('tekst_przycisku_sekcja_z_kontaktem_Allpage','options') ?></a>
                </div>
            </div>
        </div>
    </section>

</main>
















<?php get_footer() ?>