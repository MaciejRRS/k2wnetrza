<?php 
/* 
Template Name: Slajder zdjęć 
*/ 
?>

<?php get_header('default') ?>
<main id="sliderPage">
    <section class="section-slider">
        <!-- Swiper -->
        <div class="swiper-content">
            <div class="container-slider">
                <div class="swiper mySwiper-sliderPage">
                    <div class=" swiper-wrapper">
                        <?php
                    if( have_rows('lista_pageSlide') ):
                        while( have_rows('lista_pageSlide') ) : the_row(); ?>
                        <div class="swiper-slide">
                            <div class="slider-wrapper">

                                <?php $SliderImg = get_sub_field('img_item_pageSlide'); ?>
                                <img class="img-slide-item" src="<?php echo $SliderImg['sizes']['thumb-fullhd']; ?>"
                                    width="<?php echo $SliderImg['sizes']['thumb-fullhd-width']; ?>"
                                    height="<?php echo $SliderImg['sizes']['thumb-fullhd-height']; ?>"
                                    alt="<?php echo esc_attr($SliderImg['alt']); ?>" />
                            </div>
                        </div>
                        <?php
                    endwhile;
                    else :
                    endif;
                    ?>

                    </div>

                </div>

                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>

            </div>
        </div>
    </section>




</main>





<?php get_footer('default') ?>