<?php 
/* 
Template Name: Strona tekstowa
*/ 
?>

<?php get_header() ;?>

<main id="default-page">

    <div class="container">
        <h1><?php the_title(); ?></h1>
        <?php the_content(); ?>
    </div>

</main>


<?php get_footer();?>