<?php

// Register Custom Navigation Walker
require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';


/* Enable support for custom logo. */
add_theme_support( 'custom-logo', array(
		'height'      => 400,
		'width'       => 100,
		'flex-height' => true,
		'flex-width'  => true,
	) );


register_nav_menus( array(
	'primary' => __( 'Primary Menu', 'k2wnetrza' ),
) );

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function special_nav_class ($classes, $item) {
  if (in_array('current-menu-item', $classes) ){
    $classes[] = 'active ';
  }
  return $classes;
}


function add_stylesheets_and_scripts()
{
wp_enqueue_style( 'style', get_stylesheet_uri(), array(), filemtime(get_template_directory() . '/style.css'), 'all' );
wp_enqueue_style('custom', get_template_directory_uri() . '/assets/dist/css/main.min.css', array(), filemtime(get_template_directory() . '/assets/dist/css/main.min.css'), 'all' );

wp_enqueue_script('jquery', get_template_directory_uri() . '/assets/src/js/libr/jquery.min.js', array(), null, true);
// wp_enqueue_script('popper', get_template_directory_uri() . '/assets/src/js/libr/popper.min.js', array(), null, true);
wp_enqueue_script('bootstrap', get_template_directory_uri() . '/assets/src/js/libr/bootstrap.bundle.min.js', array(), null, true);
wp_enqueue_script( 'main', get_template_directory_uri() . '/assets/src/js/main.js', array(), null, true );

global $template;

// only for o-firmie.php hompage.php, 

if ((basename($template) === 'o-firmie.php') || (basename($template) === 'hompage.php') || (basename($template) === 'sliderImages.php') ){
    wp_enqueue_style( 'swiper-css', get_template_directory_uri() . '/assets/src/css/swiper/swiper-bundle.min.css' );
    wp_enqueue_script( 'swiper-js', get_template_directory_uri() . '/assets/src/js/swiper/swiper-bundle.min.js', array(), null, true );
    wp_enqueue_script( 'sctipts-about', get_template_directory_uri() . '/assets/src/js/swiper/scripts-about.js', array(), null, true );
}

// add animate AOS for all pages
    wp_enqueue_style( 'aos-css', get_template_directory_uri() . '/assets/src/css/aos.css' );
    wp_enqueue_script( 'aos-js', get_template_directory_uri() . '/assets/src/js/aos.js', array(), null, true );


}
add_action('wp_enqueue_scripts', 'add_stylesheets_and_scripts');


// Register thumbnails
	add_theme_support( 'post-thumbnails' );
	the_post_thumbnail( 'full' );
    add_image_size( 'homepage-thumb', 385, 302 ); // Soft Crop Mode
	add_image_size( 'popup-thumb-big', 767, 360 ); // Soft Crop Mode
	add_image_size( 'thumb-fullhd', 1920, 1080 ); // Soft Crop Mode
    
//add option page to panel (ACF)
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page('Stopka');
	acf_add_options_page('Popup z kontaktem');
	acf_add_options_page('Usługi - apla pod filmem');
	acf_add_options_page('Ikony - pozostałe');
}


// delete p from contact form 7
add_filter('wpcf7_autop_or_not', '__return_false');


//** *Enable upload for webp image files.*/
function webp_upload_mimes($existing_mimes) {
    $existing_mimes['webp'] = 'image/webp';
    return $existing_mimes;
}
add_filter('mime_types', 'webp_upload_mimes');

//** * Enable preview / thumbnail for webp image files.*/
function webp_is_displayable($result, $path) {
    if ($result === false) {
        $displayable_image_types = array( IMAGETYPE_WEBP );
        $info = @getimagesize( $path );

        if (empty($info)) {
            $result = false;
        } elseif (!in_array($info[2], $displayable_image_types)) {
            $result = false;
        } else {
            $result = true;
        }
    }

    return $result;
}
add_filter('file_is_displayable_image', 'webp_is_displayable', 10, 2);

function filter_acf_the_content( $value ) {
if ( class_exists( 'iworks_orphan' )) {
$orphan = new iworks_orphan();
$value = $orphan->replace( $value );
}
 
return $value;
};





// Dodawanie nowego typu postów Usługi (Usługi) START 
// function create_uslugi_posttype(){

//     $labels = array(
// 			'name' => __('uslugi'),
// 			'singular_name' => __('uslugi'),
// 			'add_new' => __('Dodaj uslugę'),
// 			'add_new_item' => __('Dodaj Nową uslugę'),
// 			'edit_item' => __('Edytuj uslugę'),
// 			'new_item' => __('Nowa usluga'),
// 			'view_item' => __('Zobacz uslugę'),
// 			'search_items' => __('Szukaj uslugi'),
// 			'not_found' => __('Nie Znaleziono uslugi'),
// 			'not_found_in_trash' => __('Brak uslugi w koszu'),
// 			'all_items' => __('Wszystkie uslugi'),
// 			'archives' => __('Zarchiwizowane uslugi'),
// 			'insert_into_item' => __('Dodaj do uslugi'),
// 			'uploaded_to_this_item' => __('Sciągnięto do bieżącej uslugi'),
// 			'featured_image' => __('Zdjęcie uslugi'),
// 			'set_featured_image' => __('Ustaw Zdjęcie uslugi'),
// 			'remove_featured_image' => __('Usuń Zdjęcie uslugi'),
// 			'use_featured_image' => __('Użyj Zdjęcie uslugi'),
// 			'menu_name' => __('uslugi')
//     );


// 	$args = array(
// 		'label' => 'uslugi',
// 		'labels' => $labels,
// 		'description' => __('Typ Postu zawiera treść dla Porad.'),
// 		'public' => true,
// 		'exclude_from_search' => false,
// 		'publicly_queryable' => true,
// 		'show_ui' => true,
// 		'show_in_nav_menus' => true,
// 		'show_in_menu' => true,
// 		'show_in_rest' => true,
// 		'show_in_admin_bar' => true,
// 		'menu_position' => 18,
// 		'menu_icon' => 'dashicons-screenoptions',
// 		'supports' => array('title','editor','author','thumbnail','excerpt','revisions','page-attributes', 'custom-fields'),
// 		'has_archive' => false,
// 		'hierarchical' => true,
// 		'rewrite' => array('slug'=>'uslugi','with_front'=>true),
// 		'capabilities' => array(
// 			'edit_post'          => 'update_core',
// 			'read_post'          => 'update_core',
// 			'delete_post'        => 'update_core',
// 			'edit_posts'         => 'update_core',
// 			'edit_others_posts'  => 'update_core',
// 			'delete_posts'       => 'update_core',
// 			'publish_posts'      => 'update_core',
// 			'read_private_posts' => 'update_core'
// 		),
// 	);
// 	register_post_type('uslugi', $args);
// }

// add_action('init','create_uslugi_posttype', 0);

// function register_taxonomy_uslugi_category() {
//     $labels = [
//         'name'              => _x('Categories', 'taxonomy general name'),
//         'singular_name'     => _x('Category', 'taxonomy singular name'),
//         'search_items'      => __('Search Categories'),
//         'all_items'         => __('All Categories'),
//         'parent_item'       => __('Parent Category'),
//         'parent_item_colon' => __('Parent Category:'),
//         'edit_item'         => __('Edit Category'),
//         'update_item'       => __('Update Category'),
//         'add_new_item'      => __('Add New Category'),
//         'new_item_name'     => __('New Category Name'),
//         'menu_name'         => __('Kategorie'),
//         ];

//         $args = [
//         'hierarchical'      => true, // make it hierarchical (like categories)
//         'labels'            => $labels,
// 		'show_ui'           => true,
// 		'label'             => 'My Taxonomy',
//         'show_admin_column' => true,
// 		'query_var'         => true,
// 		'show_ui' => true,
// 		'show_in_rest' => true,
// 		'show_in_nav_menus' => true,
// 		'show_in_admin_bar' => true,
//         'rewrite'           => ['slug' => 'kategoria'],
//         ];

//         register_taxonomy('kategoria', ['uslugi'], $args);
// }
// add_action('init', 'register_taxonomy_uslugi_category');
// Dodawanie nowego typu postów END








// Dodawanie nowego typu postów Usługi (Realizacje) START 
// function create_realizacje_posttype(){

//     $labels = array(
// 			'name' => __('realizacje'),
// 			'singular_name' => __('realizacje'),
// 			'add_new' => __('Dodaj realizację'),
// 			'add_new_item' => __('Dodaj Nową realizację'),
// 			'edit_item' => __('Edytuj realizację'),
// 			'new_item' => __('Nowa realizacja'),
// 			'view_item' => __('Zobacz realizację'),
// 			'search_items' => __('Szukaj realizacji'),
// 			'not_found' => __('Nie Znaleziono realizacji'),
// 			'not_found_in_trash' => __('Brak realizacji w koszu'),
// 			'all_items' => __('Wszystkie realizacje'),
// 			'archives' => __('Zarchiwizowane realizacje'),
// 			'insert_into_item' => __('Dodaj do realizacji'),
// 			'uploaded_to_this_item' => __('Sciągnięto do bieżącej realizacji'),
// 			'featured_image' => __('Zdjęcie realizacji'),
// 			'set_featured_image' => __('Ustaw Zdjęcie realizacji'),
// 			'remove_featured_image' => __('Usuń Zdjęcie realizacji'),
// 			'use_featured_image' => __('Użyj Zdjęcia realizacji'),
// 			'menu_name' => __('realizacje')
//     );


// 	$args = array(
// 		'label' => 'realziacje',
// 		'labels' => $labels,
// 		'description' => __('Typ Postu zawiera treść dla realizacji.'),
// 		'public' => true,
// 		'exclude_from_search' => false,
// 		'publicly_queryable' => true,
// 		'show_ui' => true,
// 		'show_in_nav_menus' => true,
// 		'show_in_menu' => true,
// 		'show_in_rest' => true,
// 		'show_in_admin_bar' => true,
// 		'menu_position' => 18,
// 		'menu_icon' => 'dashicons-screenoptions',
// 		'supports' => array('title','editor','author','thumbnail','excerpt','revisions','page-attributes', 'custom-fields'),
// 		'has_archive' => false,
// 		'hierarchical' => true,
// 		'rewrite' => array('slug'=>'realizacje','with_front'=>true),
// 		'capabilities' => array(
// 			'edit_post'          => 'update_core',
// 			'read_post'          => 'update_core',
// 			'delete_post'        => 'update_core',
// 			'edit_posts'         => 'update_core',
// 			'edit_others_posts'  => 'update_core',
// 			'delete_posts'       => 'update_core',
// 			'publish_posts'      => 'update_core',
// 			'read_private_posts' => 'update_core'
// 		),
// 	);
// 	register_post_type('realizacje', $args);
// }

// add_action('init','create_realizacje_posttype', 0);

// function register_taxonomy_realizacje_category() {
//     $labels = [
//         'name'              => _x('Categories', 'taxonomy general name'),
//         'singular_name'     => _x('Category', 'taxonomy singular name'),
//         'search_items'      => __('Search Categories'),
//         'all_items'         => __('All Categories'),
//         'parent_item'       => __('Parent Category'),
//         'parent_item_colon' => __('Parent Category:'),
//         'edit_item'         => __('Edit Category'),
//         'update_item'       => __('Update Category'),
//         'add_new_item'      => __('Add New Category'),
//         'new_item_name'     => __('New Category Name'),
//         'menu_name'         => __('Kategorie'),
//         ];

//         $args = [
//         'hierarchical'      => true, // make it hierarchical (like categories)
//         'labels'            => $labels,
// 		'show_ui'           => true,
// 		'label'             => 'My Taxonomy',
//         'show_admin_column' => true,
// 		'query_var'         => true,
// 		'show_ui' => true,
// 		'show_in_rest' => true,
// 		'show_in_nav_menus' => true,
// 		'show_in_admin_bar' => true,
//         'rewrite'           => ['slug' => 'kategoria'],
//         ];

//         register_taxonomy('kategoria', ['realizacje'], $args);
// }
// add_action('init', 'register_taxonomy_realizacje_category');
// Dodawanie nowego typu postów END


// REGISTER CPT REALIZACJE
function realizacje_post_type() {
    $labels = array(
        'name'                => 'realizacje',
        'singular_name'       => 'realizacje',
        'menu_name'           => 'realizacje',
        'parent_item_colon'   => 'Nadrzędne realizacje',
        'all_items'           => 'Wszystkie realizacje',
        'view_item'           => 'Zobacz realizacje',
        'add_new_item'        => 'Dodaj realizacje',
        'add_new'             => 'Dodaj nową realizację',
        'edit_item'           => 'Edytuj realizację',
        'update_item'         => 'Aktualizuj',
        'search_items'        => 'Szukaj realizacji',
        'not_found'           => 'Nie znaleziono',
        'not_found_in_trash'  => 'Nie znaleziono'
    ); 
    $args = array(
        'label' => 'realizacje',
        'rewrite' => array(
            'slug' => 'realizacje'
        ),
        'description'         => 'realizacje',
        'labels'              => $labels,
        'supports' => array('title','editor','author','thumbnail','excerpt','revisions','page-attributes'),
        'taxonomies'          => array(),
        'hierarchical'        => false,
        'public'              => true, 
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 4,
        'menu_icon'           => 'dashicons-id-alt',
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
    );
    register_post_type( 'realizacje', $args );
} 
add_action( 'init', 'realizacje_post_type', 0 );







function register_taxonomy_realizacje_category() {
    $labels = [
        'name'              => _x('Categories', 'taxonomy general name'),
        'singular_name'     => _x('Category', 'taxonomy singular name'),
        'search_items'      => __('Search Categories'),
        'all_items'         => __('All Categories'),
        'parent_item'       => __('Parent Category'),
        'parent_item_colon' => __('Parent Category:'),
        'edit_item'         => __('Edit Category'),
        'update_item'       => __('Update Category'),
        'add_new_item'      => __('Add New Category'),
        'new_item_name'     => __('New Category Name'),
        'menu_name'         => __('Kategorie'),
        ];
        $args = [
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
		'show_admin_column' => true,
		'show_in_rest' 		=> true,
		'query_var'         => true,
		'has_archive'		=> true,
		'rewrite'           => ['slug' => 'kategoria',
								'hierarchical' => true,
								'with_front' => true],
        ];
        register_taxonomy('kategoria', array('realizacje'), $args);
}
add_action('init', 'register_taxonomy_realizacje_category');

// Dodawanie nowego typu REALIZACJE postów END






// ------- USŁUGI -----------


// REGISTER CPT USŁUGI
function uslugi_post_type() {
    $labels = array(
        'name'                => 'usługi',
        'singular_name'       => 'usługi',
        'menu_name'           => 'usługi',
        'parent_item_colon'   => 'Nadrzędne usługi',
        'all_items'           => 'Wszystkie usługi',
        'view_item'           => 'Zobacz usługi',
        'add_new_item'        => 'Dodaj usługi',
        'add_new'             => 'Dodaj nową usługę',
        'edit_item'           => 'Edytuj usługę',
        'update_item'         => 'Aktualizuj',
        'search_items'        => 'Szukaj usługi',
        'not_found'           => 'Nie znaleziono',
        'not_found_in_trash'  => 'Nie znaleziono'
    ); 
    $args = array(
        'label' => 'uslugi',
        'rewrite' => array(
            'slug' => 'uslugi'
        ),
        'description'         => 'uslugi',
        'labels'              => $labels,
        'supports' => array('title','editor','author','thumbnail','excerpt','revisions','page-attributes'),
        'taxonomies'          => array(),
        'hierarchical'        => false,
        'public'              => true, 
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 4,
        'menu_icon'           => 'dashicons-id-alt',
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
    );
    register_post_type( 'uslugi', $args );
} 
add_action( 'init', 'uslugi_post_type', 0 );







function register_taxonomy_uslugi_category() {
    $labels = [
        'name'              => _x('Categories', 'taxonomy general name'),
        'singular_name'     => _x('Category', 'taxonomy singular name'),
        'search_items'      => __('Search Categories'),
        'all_items'         => __('All Categories'),
        'parent_item'       => __('Parent Category'),
        'parent_item_colon' => __('Parent Category:'),
        'edit_item'         => __('Edit Category'),
        'update_item'       => __('Update Category'),
        'add_new_item'      => __('Add New Category'),
        'new_item_name'     => __('New Category Name'),
        'menu_name'         => __('Kategorie'),
        ];
        $args = [
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
		'show_admin_column' => true,
		'show_in_rest' 		=> true,
		'query_var'         => true,
		'has_archive'		=> true,
		'rewrite'           => ['slug' => 'kategoria_uslug',
								'hierarchical' => true,
								'with_front' => true],
        ];
        register_taxonomy('kategoria_uslug', array('uslugi'), $args);
}
add_action('init', 'register_taxonomy_uslugi_category');

// Dodawanie nowego typu postów END












// deregister css and js contact from 7 (scripts working only for page id 22, 65)
add_action( 'wp_print_scripts', 'deregister_cf7_javascript', 100 );
function deregister_cf7_javascript() {
    if ( !is_page(array(22,65,9501,9525)) ) {
        wp_deregister_script( 'contact-form-7' );
    }
}
add_action( 'wp_print_styles', 'deregister_cf7_styles', 100 );
function deregister_cf7_styles() {
    if ( !is_page(array(22,65,9501,9525)) ) {
        wp_deregister_style( 'contact-form-7' );
    }
}



// deregister css and js reCaptcha v3 (scripts working only for page id 22, 65)
function oiw_disable_recaptcha_badge_post(){
	if ( !is_page(array(22,65,9501,9525) ) ) {
	   wp_dequeue_script('google-recaptcha');
	   add_filter( 'wpcf7_load_js', '__return_false' );
	   add_filter( 'wpcf7_load_css', '__return_false' );
	   remove_action( 'wp_enqueue_scripts', 'wpcf7_recaptcha_enqueue_scripts', 20 );
	}
 }
 add_action( 'wp_enqueue_scripts', 'oiw_disable_recaptcha_badge_post' );




// deregister search & filter css i js
function remove_sf_scripts() {
	if ( !is_page(array(15,8727,9541) ) ) {
		wp_deregister_script( 'search-filter-plugin-build' );
		wp_deregister_script( 'search-filter-plugin-chosen' );
		wp_deregister_script( 'jquery-ui-datepicker' );
		wp_deregister_style( 'search-filter-plugin-styles' );
	}
}
add_action('wp_enqueue_scripts', 'remove_sf_scripts', 100);








add_filter('bcn_breadcrumb_url', 'my_breadcrumb_url_changer', 3, 10);
function my_breadcrumb_url_changer($url, $type, $id)
{
    if(in_array('sections', $type))
    {
        $url = str_replace("sections/", "", $url);
    }
    return $url;
}



 // debug for worpdress
ini_set('display_errors','Off');
ini_set('error_reporting', E_ALL );
define('WP_DEBUG', false);
define('WP_DEBUG_DISPLAY', false);