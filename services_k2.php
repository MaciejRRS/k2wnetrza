<?php 
/* 
Template Name: Nasze usługi
*/ 
?>

<?php get_header() ;?>

<main id="services">
    <section class="hero hero-subpage">
        <div class="container">
            <div class="hero-wrapper">
                <div class="column-left" data-aos="fade-right" data-aos-duration="1000">
                    <h1><?php the_field('tytul_sekcja_1_services') ?></h1>
                    <?php the_field('tekst_sekcja_1_services') ?>
                    <div class="buttons-wrapper">
                        <?php if( get_field('przycisk_nazwa_sekcja_1_services') ): ?>
                        <a href="<?php the_field('przycisk_link_sekcja_1_services') ?>"
                            class="btn btn-grad"><?php the_field('przycisk_nazwa_sekcja_1_services') ?></a>
                        <?php endif; ?>
                        <?php if( get_field('przycisk2_nazwa_sekcja_1_services') ): ?>
                        <a href="<?php the_field('przycisk_link2_sekcja_1_services') ?>"
                            class="btn btn-grad bgWhite"><?php the_field('przycisk2_nazwa_sekcja_1_services') ?></a>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="column-right" data-aos="fade-left" data-aos-duration="1000">
                    <?php $imageHeroHomepage = get_field('zdjecie_sekcja_1_services'); ?>
                    <img class="img-hero-homepage" src="<?php echo $imageHeroHomepage['sizes']['large']; ?>"
                        width="<?php echo $imageHeroHomepage['sizes']['large-width']; ?>"
                        height="<?php echo $imageHeroHomepage['sizes']['large-height']; ?>"
                        alt="<?php echo esc_attr($imageHeroHomepage['alt']); ?>" />
                </div>

            </div>
            <div class="breadcrumps">
                <?php if( function_exists( 'bcn_display' ) ) bcn_display(); ?>
            </div>
        </div>

        <a href="#co-wyroznia-nasz-wirtualny-spacer" class="arrow-icon-scroll">
            <div class="arrow-area">
                <?php the_field('scroll_napis','options') ?> <i class="arrow down"></i>
            </div>
        </a>


    </section>




    <section id="co-wyroznia-nasz-wirtualny-spacer" class="section_no_image" data-aos="fade-up"
        data-aos-duration="1000">
        <div class="section_no_image_wrapper">
            <div class="container">
                <div class="title-area">
                    <h2><?php the_field('tytul_sekcja_bez_zdjecia_service') ?></h2>
                </div>
                <div class="text-area">
                    <?php the_field('tekst_sekcja_bez_zdjecia_service') ?>
                </div>
            </div>
        </div>
    </section>






    <?php

$custom_query = new WP_Query( 
    array(
    'post_type' => 'uslugi',
    'post_status'=>'publish',
    'posts_per_page' => -1,
    ) 
);
$i = 3;
?>

    <?php if ( $custom_query->have_posts() ) : while  ( $custom_query->have_posts() ) : $custom_query->the_post(); ?>

    <?php
if( get_field('wlacznik_sekcji_usługa_lista_services') ) { ?>
    <section class="section-repeater section-repeater-<?php echo $i++; ?>">
        <div class="container">
            <div class="wrapper-repeater" data-aos="fade-up" data-aos-duration="1000">
                <div class="column-1">
                    <?php $realization_excerpt = get_the_excerpt($post->ID);?>
                    <?php $urlImgServices = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'large' ); ?>


                    <?php
                // $spacer_zdjecie_home = get_sub_field('zdjecie_blok_repeater_'); 
                $spacer_film_list_serv = get_field('film_blok_repeater_list_services'); 
                $get_selection_list_serv = get_field('wybor_zdjecie_film_blok_repeater_list_services'); ?>

                    <?php if ($get_selection_list_serv == 'zdjecie'){ ?>
                    <a href="<?php echo get_permalink(); ?>">
                        <?php the_post_thumbnail('large' , array( 'class' => 'HomeRepeaterImg' )); ?>

                    </a>
                    <?php  } else if ( $get_selection_list_serv == 'film') { ?>
                    <div class="movie-area popup-area">
                        <?php the_field('film_blok_repeater_list_services'); ?>
                    </div>
                    <img class="img-apla-serv"
                        src="<?php the_field('tlo_pod_filmem_blok_services_single','options') ?>">



                    <!-- start popup -->
                    <?php  } else if ($get_selection_list_serv == 'popup') { ?>
                    <div class="movie-area popup-area btn-noAnimate">
                        <?php $services_listRepeaterImgPopup = get_field('zdjecie_popup_blok_repeater_services_list'); ?>
                        <div id="myBtn" class="popup-clicable-area" data-toggle="modal"
                            data-target="#modal-box-<?php echo $i++; ?>">
                            <div class="icon-play">
                                <img src="<?php the_field('wybierz_ikonę_linku_popup','options') ?>">
                            </div>
                            <img class="HomeRepeaterImg"
                                src="<?php echo $services_listRepeaterImgPopup['sizes']['large']; ?>"
                                width="<?php echo $services_listRepeaterImgPopup['sizes']['large-width']; ?>"
                                height="<?php echo $services_listRepeaterImgPopup['sizes']['large-height']; ?>"
                                alt="<?php echo esc_attr($services_listRepeaterImgPopup['alt']); ?>" />
                        </div>
                        <div id="modal-box-<?php echo --$i; ?>" class="modal">

                            <!-- Modal content -->
                            <div id="mymodal-close" class="modal-content">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <iframe src="<?php the_field('popup_content_repeater_services_list_content'); ?>"
                                    frameborder="0"></iframe>
                            </div>

                        </div>

                    </div>
                    <img class="img-apla-serv"
                        src="<?php the_field('tlo_pod_filmem_blok_services_single','options') ?>">
                    <?php  } ?>

                    <!-- end popup -->

                </div>
                <div class="column-2">
                    <a href="<?php echo get_permalink(); ?>">
                        <div class="title-area">
                            <h2><?php the_title(); ?></h2>
                        </div>
                        <div class="text-area">
                            <?php echo '<p>'.$realization_excerpt.'</p>' ?>
                        </div>
                    </a>
                    <!-- zmieniamy acfy dla przycisków w liście usług -->
                    <div class="btn-area">
                        <?php if( get_field('przycisk_tekst_sprawdz_oferte_list_of_all') ): ?>
                        <a href="<?php echo get_permalink(); ?>"
                            class="btn-grad btn-repeater"><?php the_field('przycisk_tekst_sprawdz_oferte_list_of_all') ?></a>
                        <?php endif; ?>
                        <?php if( get_field('przycisk_tekst_sprawdz_oferte_list_of_all') ): ?>
                        <a href="<?php the_field('przycisk_link_sprawdz_realizacje_list_of_all');?>"
                            class="btn-grad btn-repeater bgWhite"><?php the_field('przycisk_tekst_sprawdz_realizacje_list_of_all') ?></a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <?php } ?>

    <?php
endwhile; 
endif; 
wp_reset_query();
?>



    <!-- <section class="section-repeater section-repeater-2 change-side">
        <div class="container">
            <div class="wrapper-repeater">
                <div class="column-1" data-aos="fade-up" data-aos-duration="1000">
                    <a href="<?php the_field('link_przycisku_blokow_repeater_service_one') ?>">
                        <?php $serviceOneRepeaterImg = get_field('zdjecie_blok_repeater_service_one'); ?>
                        <img class="HomeRepeaterImg" src="<?php echo $serviceOneRepeaterImg['sizes']['large']; ?>"
                            width="<?php echo $serviceOneRepeaterImg['sizes']['large']; ?>"
                            height="<?php echo $serviceOneRepeaterImg['sizes']['large']; ?>"
                            alt="<?php echo esc_attr($serviceOneRepeaterImg['alt']); ?>" />
                    </a>
                </div>
                <div class="column-2" data-aos="zoom-in" data-aos-duration="1000">
                    <a href="<?php the_field('link_przycisku_blokow_repeater_service_one') ?>">
                        <div class="title-area">
                            <h2><?php the_field('tytul_blok_repeater_service_one') ?></h2>
                        </div>
                        <div class="text-area">
                            <?php the_field('tekst_blok_repeater_service_one') ?>
                        </div>
                    </a>
                    <?php if( get_field('tekst_przycisku_blokow_repeater_service_one') ): ?>
                    <div class="btn-area">
                        <a href="<?php the_field('link_przycisku_blokow_repeater_service_one') ?>"
                            class="btn-grad btn-repeater"><?php the_field('tekst_przycisku_blokow_repeater_service_one') ?></a>
                    </div>
                    <?php endif; ?>
                </div>
            </div>

        </div>
    </section> -->



    <section class="cta-home" data-aos="zoom-in" data-aos-duration="1000">
        <div class="cta-wrapper">
            <div class="container">
                <div class="title-area">
                    <h2><?php the_field('tytul_sekcja_z_kontaktem_Allepage','options') ?></h2>
                </div>
                <div class="text-area">
                    <?php the_field('tekst_sekcja_z_kontaktem_Allpage','options') ?>
                </div>
                <div class="btn-area">
                    <a href="<?php the_field('link_przycisku_sekcja_z_kontaktem_Allpage','options') ?>"
                        class="btn btn-grad"><?php the_field('tekst_przycisku_sekcja_z_kontaktem_Allpage','options') ?></a>
                </div>
            </div>
        </div>
    </section>


</main>


<?php get_footer();?>