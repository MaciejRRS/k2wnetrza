<?php 
/* 
Template Name: Pusty Szablon
*/ 
?>

<?php get_header() ;?>

<main id="blank-template">
    <section class="section-content">
        <?php while(have_posts()) : the_post(); ?>
        <div class="container">
            <?php the_content();?>
        </div>
        <?php endwhile; ?>
    </section>
</main>

<?php get_footer() ;?>