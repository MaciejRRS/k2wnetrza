<footer style="background-image: url(/app/themes/k2wnetrza/assets/src/img/footer-animate2.svg);" id="footer">
    <div class="footer-wrapper">
        <div class="container">
            <div class="footer-content">
                <div class="footer-logo">
                    <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>"
                        title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"> <img
                            class="footer-logo" src="<?php the_field('logo_stopka_white','options') ?>" alt=""></a>

                </div>

                <div class="wrapper-center-columns">
                    <div class="column-f column-footer-1">
                        <?php
                if( have_rows('kolumna1_stopka_lista1','options') ):
                    while( have_rows('kolumna1_stopka_lista1','options') ) : the_row(); ?>
                        <a href="<?php the_sub_field('link_zakladki_footer_column1','options');  ?>"><img
                                src="<?php the_sub_field('ikona_footer_column1','options') ?>"><?php the_sub_field('tekst_zakladki_footer_column1','options');  ?></a>
                        <?php
                endwhile;
                else :
                endif; ?>
                    </div>
                    <div class="column-f column-footer-2">
                        <?php
                if( have_rows('kolumna2_stopka_lista','options') ):
                    while( have_rows('kolumna2_stopka_lista','options') ) : the_row(); ?>
                        <a
                            href="<?php the_sub_field('link_zakladki_footer_column2','options');  ?>"><?php the_sub_field('tekst_zakladki_footer_column2','options');  ?></a>
                        <?php
                endwhile;
                else :
                endif; ?>
                    </div>
                    <div class="column-f column-footer-3">
                        <?php
                if( have_rows('kolumna3_stopka_lista','options') ):
                    while( have_rows('kolumna3_stopka_lista','options') ) : the_row(); ?>
                        <a
                            href="<?php the_sub_field('link_zakladki_footer_column3','options');  ?>"><?php the_sub_field('tekst_zakladki_footer_column3','options');  ?></a>
                        <?php
                endwhile;
                else :
                endif; ?>
                    </div>
                </div>


                <div class="column-footer-4">
                    <a href="<?php the_field('przycisk_link_spacer_footer','options') ?>"
                        class="btn-grad"><?php the_field('przycisk_tekst_spacer_footer','options') ?></a>
                </div>
            </div>

            <div class="footer-rr">
                <a href=" https://redrocks.pl">
                    <p> Projekt i realizacja: RedRockS - Agencja Kreatywna </p>
                </a>
                <p> ⓒ <?php echo date("Y"); ?> All rights reserved </p>
            </div>
        </div>
    </div>
    </div>

</footer>

<?php wp_footer(); ?>




<script>
jQuery(".btn-noAnimate").click(function() {
    jQuery(".wrapper-repeater").toggleClass("animated");
});
jQuery("#close-1").click(function() {
    $(".wrapper-repeater").removeClass("animated");
});
</script>
<script>
jQuery(document).ready(function() {
    jQuery('#header-fixed-btn').on('click', function() {
        jQuery('#fixed-popup').toggleClass("fixed-open");
    });
});
jQuery(document).ready(function() {
    jQuery('#fixed-close').on('click', function() {
        jQuery('#fixed-popup').removeClass("fixed-open");
    });
});
</script>
<script>
// Get the modal
window.onload = function() {
    var modal = document.getElementById("myModal");
    // Get the button that opens the modal
    var btn = document.getElementById("myBtn");
    // Get the <span> element that closes the modal
    var span = document.getElementById("close-1");
    var blok1 = document.getElementById("blok-1");
    // When the user clicks the button, open the modal
    btn.onclick = function() {
        modal.style.display = "block";
    }
    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
        blok1.classList.remove("animated");
    }
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
            var el = document.querySelectorAll(".wrapper-repeater");
            el.forEach(function(Item) {
                Item.classList.remove("animated");
            });
        }
    }
}
</script>
<!-- script animation -->
<!-- edn script animation -->
<script>
AOS.init();
</script>
<script>
var titleMessageGrass = '<?php echo the_field('tabs_title_message_grass', 'options'); ?>';
var titleMessageHeart = '<?php echo the_field('tabs_title_message_heart', 'options'); ?>';
jQuery(function() {
    var message = titleMessageHeart;
    var original;
    jQuery(window).focus(function() {
        if (original) {
            document.title = original;
        }
    }).blur(function() {
        var title = jQuery('title').text();
        if (title != message) {
            original = title;
        }
        document.title = message;
    });
});
</script>
<!-- Messenger Wtyczka czatu Code -->
<!-- <div id="fb-root"></div>

<div id="fb-customer-chat" class="fb-customerchat">
</div>
<script>
var chatbox = document.getElementById('fb-customer-chat');
chatbox.setAttribute("page_id", "950520871778385");
chatbox.setAttribute("attribution", "biz_inbox");
</script>

<script>
window.fbAsyncInit = function() {
    FB.init({
        xfbml: true,
        version: 'v12.0'
    });
};
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = 'https://connect.facebook.net/pl_PL/sdk/xfbml.customerchat.js';
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script> -->

</body>

</html>