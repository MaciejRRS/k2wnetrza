<?php 
/* 
Template Name: Faq 
*/ 
?>

<?php get_header() ?>
<main id="faq">
    <section class="hero">
        <div class="container">

            <div class="title-site-wrapper">
                <div class="wrapper-titleAndText">
                    <h1><?php the_title(); ?></h1>
                    <?php the_content(); ?>
                </div>
                <div class="breadcrumps">
                    <?php if( function_exists( 'bcn_display' ) ) bcn_display(); ?>
                </div>
            </div>
        </div>
    </section>


    <section class="content-wrapper-faq">
        <div class="container">
            <?php
if( have_rows('lista_pytan_i_odpowiedzi_faq') ):
    while( have_rows('lista_pytan_i_odpowiedzi_faq') ) : the_row(); ?>
            <details data-aos="fade-up" data-aos-duration="1000">
                <summary>
                    <h2><?php the_sub_field('pytanie_faq'); ?></h2>
                </summary>
                <?php the_sub_field('odpowiedz_faq'); ?>
            </details>
            <?php endwhile;
else :
endif; ?>
        </div>
    </section>




    <section class="cta-home">
        <div class="cta-wrapper">
            <div class="container">
                <div class="title-area">
                    <h2><?php the_field('tytul_sekcja_z_kontaktem_Allepage','options') ?></h2>
                </div>
                <div class="text-area">
                    <?php the_field('tekst_sekcja_z_kontaktem_Allpage','options') ?>
                </div>
                <div class="btn-area">
                    <a href="<?php the_field('link_przycisku_sekcja_z_kontaktem_Allpage','options') ?>"
                        class="btn btn-grad"><?php the_field('tekst_przycisku_sekcja_z_kontaktem_Allpage','options') ?></a>
                </div>
            </div>
        </div>
    </section>
</main>

</main>
















<?php get_footer() ?>