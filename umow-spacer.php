<?php 
/* 
Template Name: Umów się na spacer
*/ 
?>

<?php get_header() ?>


<main id="umow-spacer">
    <div class="container">

        <div class="columns-row">

            <div class="column-left" data-aos="fade-right" data-aos-duration="1000">
                <div class="title-area">
                    <h1><?php the_field('tytul_strony_umow_sie_na_spacer') ?></h1>
                </div>
                <div class="text-area">
                    <?php the_field('teskt_strony_umow_sie_na_spacer') ?>
                </div>
                <div class="buttons-area">
                    <a class="btn-grad btn-1" href="<?php the_field('link_przycisku1_umow_sie_na_spacer') ?>"><img
                            src="<?php the_field('ikona_przycisku1_umow_sie_na_spacer') ?>"
                            class="contact-img"><?php the_field('tekst_przycisku1_umow_sie_na_spacer') ?></a>
                    <a class="btn-grad btn-2" href="<?php the_field('link_przycisku2_umow_sie_na_spacer') ?>"><img
                            src="<?php the_field('ikona_przycisku2_umow_sie_na_spacer2') ?>"
                            class="contact-img"><?php the_field('tekst_przycisku2_umow_sie_na_spacer') ?></a>

                </div>

                <div class="form-area">
                    <h2><?php the_field('naglowek_nad_formualrzem_umow_sie_na_spacer') ?></h2>
                    <?php echo do_shortcode(get_field('formuarz_umow_sie_na_spacer')); ?>
                </div>
            </div>

            <div class="column-right" data-aos="fade-left" data-aos-duration="1000">
                <?php $imageUmowSpecer = get_field('zdjecie_umow_sie_na_spacer'); ?>
                <img class="img-hero-umowSpacer" src="<?php echo $imageUmowSpecer['sizes']['large']; ?>"
                    width="<?php echo $imageUmowSpecer['sizes']['large']; ?>"
                    height="<?php echo $imageUmowSpecer['sizes']['large']; ?>"
                    alt="<?php echo esc_attr($imageUmowSpecer['alt']); ?>" />
            </div>
        </div>
    </div>
</main>

<?php the_field('') ?>
















<?php get_footer() ?>