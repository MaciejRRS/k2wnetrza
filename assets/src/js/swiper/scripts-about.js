var swiper = new Swiper('.mySwiper', {
    loop: true,
    slideToClickedSlide: true,
    autoplay: {
        delay: 15000,
        disableOnInteraction: false,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    breakpoints: {
        320: {
            slidesPerView: 1,
            spaceBetween: 5,
        },

        576: {
            slidesPerView: 1,
            spaceBetween: 10,
        },
        692: {
            slidesPerView: 1,
            spaceBetween: 10
        },
        1200: {
            slidesPerView: 2,
            spaceBetween: 20
        },
        1920: {
            slidesPerView: 2,
            spaceBetween: 30
        }
    },
});


var swiper = new Swiper('.mySwiper-partners', {
    loop: true,
    auto: true,
    autoplay: {
        delay: 2500,
        disableOnInteraction: false,
    },
    breakpoints: {
        320: {
            slidesPerView: 2,
            spaceBetween: 5,
        },

        576: {
            slidesPerView: 3,
            spaceBetween: 10,
        },
        692: {
            slidesPerView: 4,
            spaceBetween: 10
        },
        1199: {
            slidesPerView: 4,
            spaceBetween: 20
        },
        1920: {
            slidesPerView: 4,
            spaceBetween: 30
        }
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});

var swiper = new Swiper('.mySwiper-sliderPage', {
    loop: true,
    auto: true,
    slidesPerView: 1,
    autoplay: {
        delay: 3000,
    },
    pagination: {
        el: ".swiper-pagination",
        clickable: true,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});



