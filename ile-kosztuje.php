<?php 
/* 
Template Name: Ile to kosztuje  
*/ 
?>

<?php get_header() ?>
<main id="howCost">

    <section class="hero hero-subpage">
        <div class="container">
            <div class="hero-wrapper">
                <div class="column-left" data-aos="fade-right" data-aos-duration="1000">
                    <h1><?php the_field('tytul_sekcja_1_howCost') ?></h1>
                    <?php the_field('tekst_sekcja_1_howCost') ?>
                    <div class="buttons-wrapper">
                        <a href="<?php the_field('przycisk_link_zapytaj_o_oferte_real_howCost') ?>"
                            class="btn btn-grad"><?php the_field('przycisk_tekst_zapytaj_o_oferte_howCost') ?></a>
                    </div>
                </div>
                <div class="column-right" data-aos="fade-left" data-aos-duration="1000">
                    <?php $imageHeroHowCost = get_field('zdjecie_sekcja_1_howCost'); ?>
                    <img class="img-hero-homepage" src="<?php echo $imageHeroHowCost['sizes']['large']; ?>"
                        width="<?php echo $imageHeroHowCost['sizes']['large-width']; ?>"
                        height="<?php echo $imageHeroHowCost['sizes']['large-height']; ?>"
                        alt="<?php echo esc_attr($imageHeroHowCost['alt']); ?>" />
                </div>

            </div>
            <div class="breadcrumps">
                <?php if( function_exists( 'bcn_display' ) ) bcn_display(); ?>
            </div>
        </div>
        <!-- <a href="#section1" class="arrow-icon-scroll"><span><?php the_field('scroll_napis','options') ?></span></a> -->

        <a href="#section1" class="arrow-icon-scroll">
            <div class="arrow-area">
                <?php the_field('scroll_napis','options') ?> <i class="arrow down"></i>
            </div>
        </a>

    </section>




    <?php
if( get_field('wlacz_wylacz_sekcje_prices') ) { ?>
    <section id="section1" class="prices" data-aos="fade-up" data-aos-duration="1000">
        <div class="container">
            <div class="title-area">
                <h2><?php the_field('title_section_prices') ?></h2>
            </div>
            <div class="text-area">
                <?php the_field('text_section_prices') ?>
            </div>

            <div class="repeater-blocks-price">
                <?php
                    if( have_rows('list_section_prices') ): $i = 2;
                        while( have_rows('list_section_prices') ) : the_row(); ?>

                <?php if( get_sub_field('wlacz_wylacz_blok_prices') ) { ?>
                <div class="blok-item" data-aos="fade-up" data-aos-duration="1000">
                    <div class="img-block">
                        <?php $imgHowCost_list_img = get_sub_field('img_section_prices'); ?>
                        <img class="RepeaterImgPrice" src="<?php echo $imgHowCost_list_img['sizes']['large']; ?>"
                            width="<?php echo $imgHowCost_list_img['sizes']['large']; ?>"
                            height="<?php echo $imgHowCost_list_img['sizes']['large']; ?>"
                            alt="<?php echo esc_attr($imgHowCost_list_img['alt']); ?>" />
                    </div>
                    <div class="text-block">
                        <div class="price-tekst">
                            <h5><?php the_sub_field('price_text_section_prices') ?></h5>
                        </div>
                        <h3><?php the_sub_field('title_section_prices') ?></h3>
                        <?php the_sub_field('text_section_prices') ?>
                        <a class="btn btn-grad"
                            href="<?php the_sub_field('przycisk_link_section_prices') ?>"><?php the_sub_field('przycisk_tekst_section_prices') ?></a>
                    </div>
                </div>
                <?php } ?>

                <?php
                    endwhile;
                    else :
                    endif;
                    ?>

            </div>
        </div>
    </section>
    <?php } ?>


    <?php if( get_field('wlacz_wylacz_dodatkowa_sekcja_prices') ): ?>
    <section class="section_no_image" data-aos="fade-up" data-aos-duration="1000">
        <div class="section_no_image_wrapper">
            <div class="container">
                <div class="title-area">
                    <h2><?php the_field('tytul_dodatkowa_sekcja_prices') ?></h2>
                </div>
                <div class="text-area">
                    <?php the_field('tekst_dodatkowa_sekcja_prices') ?>
                </div>
                <div class="btn-area">
                    <a href="<?php the_field('przycisk_link_dodatkowa_sekcja_prices')?>"
                        class="btn btn-grad"><?php the_field('przycisk_tekst_dodatkowa_sekcja_prices')?></a>
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>

    <?php
if( get_sub_field('wlacz__wylacz_lista_blokow_ze_zdjeciem_prices') ) { ?>
    <section id="section1" class="list-blocksAndImg">
        <!-- wyłaczane sekcje -->
        <?php
                    if( have_rows('lista_blokow_ze_zdjeciem_prices') ): $i = 2;
                        while( have_rows('lista_blokow_ze_zdjeciem_prices') ) : the_row(); ?>



        <div class="section-repeater change-side-realization">
            <div class="container">
                <div class="wrapper-repeater" data-aos="fade-up" data-aos-duration="1000">
                    <div class="column-2">
                        <div class="title-area">
                            <h2><?php the_sub_field('tytul_lista_blokow_ze_zdjeciem_prices') ?></h2>
                        </div>
                        <div class="text-area">
                            <?php the_sub_field('tresc_lista_blokow_ze_zdjeciem_prices') ?>
                        </div>
                        <div class="btn-area">
                            <a href="<?php the_sub_field('przycisk1_link_lista_blokow_ze_zdjeciem_prices') ?>"
                                class="btn btn-grad btn-repeater"><?php the_sub_field('przycisk1_tekst_lista_blokow_ze_zdjeciem_prices') ?></a>
                            <a href="<?php the_sub_field('przycisk2_link_lista_blokow_ze_zdjeciem_prices');?>"
                                class="btn-grad btn-repeater bgWhite"><?php the_sub_field('przycisk2_tekst_lista_blokow_ze_zdjeciem_prices') ?></a>
                        </div>
                    </div>
                    <div class="column-1">
                        <?php $ListPricesImgRepeater = get_sub_field('zdjecie_link_lista_blokow_ze_zdjeciem_prices'); ?>
                        <img class="HomeRepeaterImg" src="<?php echo $ListPricesImgRepeater['sizes']['large']; ?>"
                            width="<?php echo $ListPricesImgRepeater['sizes']['large-width']; ?>"
                            height="<?php echo $ListPricesImgRepeater['sizes']['large-height']; ?>"
                            alt="<?php echo esc_attr($ListPricesImgRepeater['alt']); ?>" />
                    </div>
                </div>

            </div>
        </div>


        <?php
                    endwhile;
                    else :
                    endif;
                    ?>
    </section>
    <!-- end wyłaczane sekcje -->
    <?php } ?>



    <section class="cta-home" data-aos="fade-up" data-aos-duration="1000">
        <div class="cta-wrapper">
            <div class="container">
                <div class="title-area">
                    <h2><?php the_field('tytul_sekcja_z_kontaktem_Allepage','options') ?></h2>
                </div>
                <div class="text-area">
                    <?php the_field('tekst_sekcja_z_kontaktem_Allpage','options') ?>
                </div>
                <div class="btn-area">
                    <a href="<?php the_field('link_przycisku_sekcja_z_kontaktem_Allpage','options') ?>"
                        class="btn btn-grad"><?php the_field('tekst_przycisku_sekcja_z_kontaktem_Allpage','options') ?></a>
                </div>
            </div>
        </div>
    </section>
</main>
















<?php get_footer() ?>